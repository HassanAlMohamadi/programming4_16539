<?php
namespace ModernWays\FricFrac\Dal;

class Event extends Base {
    
    public static function delete($id) {
        $success = 0;
        if (self::connect()) {
            $Id = \ModernWays\Helpers::escape($id);
      
              try {
                $sql = 'DELETE FROM Event WHERE Id = :Id';
                $statement = self::$connection->prepare($sql);
                $statement->bindParam(':Id', $Id);
                $statement->execute();
                $success = $statement->rowCount();
                if ($success == 0) {
                    self::$message = "De rij met id $id bestaat niet!";
                } else {
                    self::$message = "De rij met id $id is gedeleted!";
                    
                }
            } catch (\PDOException $exception) {
                self::$message = $exception->getMessage();
                self::$message = "Fout: verwijderen mislukt!";
            }
         
        }
        return $success;
    }
    
    
    
    
    
    
    public static function create($post) {
        $success = false;
        if (self::connect()) {
            $newEntity = array(
                'Name'                 => \ModernWays\Helpers::escape($post['Name']),
                'Location'             => \ModernWays\Helpers::escape($post['Location']),
                'Starts'               => \ModernWays\Helpers::escape($post['Starts']),
                'Ends'                 => \ModernWays\Helpers::escape($post['Ends']),
                'Image'                => \ModernWays\Helpers::escape($post['Image']),
                'Description'          => \ModernWays\Helpers::escape($post['Description']),
                'OrganiserName'        => \ModernWays\Helpers::escape($post['OrganiserName']),
                'OrganiserDescription' => \ModernWays\Helpers::escape($post['OrganiserDescription']),
                'EventCategoryId'      => \ModernWays\Helpers::escape($post['EventCategoryId']),
                'EventTopicId'         => \ModernWays\Helpers::escape($post['EventTopicId']),
      );
            try {
                $sql = sprintf("INSERT INTO %s (%s) VALUES (:%s)", 'Event', 
                    implode(', ', array_keys($newEntity)), 
                    implode(', :', array_keys($newEntity)));
                $statement = self::$connection->prepare($sql);
                $statement->execute($newEntity);
                self::$message = 'Rij is toegevoegd in Event!';
                $success = true;
            } catch (\PDOException $exception) {
                self::$message = $exception->getMessage();
                self::$message = "Rij is niet toegevoegd in Event!";
            }
        }
        return $success;
    }
    
    
    
    
    
    
    public static function readOneByName($name) {
        $success = 0;
        if (self::connect()) {
            $Name = \ModernWays\Helpers::escape($name);
            try {
                $sql = 'SELECT * FROM Event WHERE Name = :Name';
                $statement = self::$connection->prepare($sql);
                $statement->bindParam(':Name', $name, \PDO::PARAM_STR);
                $statement->execute();
                $result = $statement->fetchAll();
                $success = $statement->rowCount();
                if ($success == 0) {
                    self::$message = "Geen land met de naam $name gevonden.";
                } else {
                    self::$message = "Alle landen met de naam $name is gevonden.";
                }
            } catch (\PDOException $exception) {
               self::$message = "Syntax fout in SQL: {$exception->getMessage()}";
             }
        } 
        return $success;
    }
    
    
    
    
    
    
        public static function readOneById($id) {
        if (self::connect()) {
            $id = \ModernWays\Helpers::escape($id);
            try {
                $sql = 'SELECT * FROM Event WHERE Id = :Id';
                $statement = self::$connection->prepare($sql);
                $statement->bindParam(':Id', $id, \PDO::PARAM_STR);
                $statement->execute();
                $result = $statement->fetch(\PDO::FETCH_ASSOC);
                if ($result) {
                    self::$message = "De rij met de id $id is ingelezen.";
                } else {
                   self::$message = "De rij met de id $id is niet ingelezen.";
                }
            } catch (\PDOException $exception) {
                self::$message = $exception->getMessage();
                self::$message = "Geen event met de id $id gevonden.";
            }
        } 
        return $result;
    }
    
    
    
    
    
    
    public static function readAll() {
        $result = null;
        if (self::connect()) {
            try {
                $sql = 'SELECT Name, Id, Starts,Location FROM Event ORDER BY Starts, Name';
                $statement = self::$connection->prepare($sql);
                $statement->execute();
                $result = $statement->fetchAll();
                self::$message = "Alle rijen van Event zijn ingelezen.";
            } catch (\PDOException $exception) {
                self::$message = $exception->getMessage();
                self::$message = "De tabel Evnt is leeg.";
            }
        } 
        return $result;
    }
    
    
    
    
    public static function Update($post) {
        $success = 0;
        if (self::connect()) {
            $updateEvent= array(
                'Id'=>                   \ModernWays\Helpers::escape($post['Id']),
                'Name'=>                 \ModernWays\Helpers::escape($post['Name']),
                'Location'=>             \ModernWays\Helpers::escape($post['Location']),
                'Starts'=>               \ModernWays\Helpers::escape($post['Starts']),
                'Ends'=>                 \ModernWays\Helpers::escape($post['Ends']),
                'Image'=>                \ModernWays\Helpers::escape($post['Image']),
                'Description'=>          \ModernWays\Helpers::escape($post['Description']),
                'OrganiserName'=>        \ModernWays\Helpers::escape($post['OrganiserName']),
                'OrganiserDescription'=> \ModernWays\Helpers::escape($post['OrganiserDescription']),
                'EventCategoryId'=>      \ModernWays\Helpers::escape($post['EventCategoryId']),
                'EventTopicId'=>         \ModernWays\Helpers::escape($post['EventTopicId'])
            );

            try {
                $sql = 'UPDATE Event SET Name = :Name, Location = :Location, Starts = :Starts, Ends = :Ends, Image = :Image,
                                         Description = :Description, OrganiserName = :OrganiserName, OrganiserDescription = :OrganiserDescription,
                                         EventCategoryId = :EventCategoryId, EventTopicId = :EventTopicId
                                         WHERE Id = :Id';
                    
                $statement= self::$connection->prepare($sql);
                $statement -> bindParam(':Id',                   $updateEvent['Id']);
                $statement -> bindParam(':Name',                 $updateEvent['Name']);
                $statement -> bindParam(':Location',             $updateEvent['Location']);
                $statement -> bindParam(':Starts',               $updateEvent['Starts']);
                $statement -> bindParam(':Image',                $updateEvent['Image']);
                $statement -> bindParam(':Description',          $updateEvent['Description']);
                $statement -> bindParam(':OrganiserName',        $updateEvent['OrganiserName']);
                $statement -> bindParam(':OrganiserDescription', $updateEvent['OrganiserDescription']);
                $statement -> bindParam(':EventCategoryId',      $updateEvent['EventCategoryId']);
                $statement -> bindParam(':EventTopicId',         $updateEvent['EventTopicId']);

                $statement->execute($updateEvent);
                
                $success = $statement->rowCount();
                
                if ($success == 0) {
                self::$message = "Het land met de naam {$updateEvent['Name']} is niet gevonden.";
                } else {
                self::$message = "Het land met de naam {$updateEvent['Name']} is geüpdated.";
                }
            } catch (\PDOException $exception) {
                self::$message = "Het land met de naam {$updateEvent['Name']} is niet geüpdated. Syntax error: { $exception->getMessage()}";
            }
        }    
        return $success;
    }
}