<?php
namespace ModernWays\FricFrac\Model;

class Person {
    
    private $id;
    private $firstName;
    private $lastName;
    private $email;
    private $address1;
    private $address2;
    private $postalCode;
    private $city;
    private $birthday;
    private $phone1;
    private $countryId;
    
    private $list;


    public function setId($value) {
        $this->id = \ModernWays\Helpers::cleanUpInput($value);
    }
    public function setFirstName($value) {
        $this->firstName = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setLastName($value) {
        $this->lastName = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setEmail($value) {
        $this->email = \ModernWays\Helpers::cleanUpInput($value);
    }

    public function setAddress1($value) {
        $this->address1 = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setAddress2($value) {
        $this->address2 = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setPostalCode($value) {
        $this->postalCode = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setCity($value) {
        $this->city = \ModernWays\Helpers::cleanUpInput($value);
    }
    
        
    public function setBirthday($value) {
        $this->birthday = $value;
    }
    
    public function setPhone1($value) {
        $this->phone1 = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setCountryId($value) {
        $this->countryId = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    
    public function getId() {
        return $this->id;
    }
    
    public function getFirstName() {
    return $this->firstName;
    }
    
    public function getLastName() {
        return $this->lastName;
    }
    
    public function getEmail() {
        return $this->email;
    }
     
    public function getAddress1() {
        return $this->address1;
    }
    
    
     public function getAddress2() {
        return $this->address2;
    }
     
    public function getBirthday() {
        $date = new \DateTime($this->birthday);
        return $date->format('Y-m-d');
    }
    
    public function getPostalCode() {
        return $this->postalCode;
    }
     
    public function getCity() {
        return $this->city;
    }
     
    public function getPhone1() {
        return $this->phone1;
    }
     
    public function getCountryId() {
        return $this->countryId;
    }
     
    
    public function toArray() {
        return array(
            "Id"         => $this->getId(),
            "FirstName"  => $this->getFirstName(),
            "LastName"   => $this->getLastName(),
            "Email"      => $this->getEmail(),
            "Address1"   => $this->getAddress1(),
            "Address2"   => $this->getAddress2(),
            "PostalCode" => $this->getPostalCode(),
            "City"       => $this->getCity(),
            "Birthday"   => $this->getBirthday(),
            "Phone1"     => $this->getPhone1(),
            "CountryId"  => $this->getCountryId()
            );
    }
    
    public function arrayToObject($array) {
        
            $this->setId        ($array['Id']);
            $this->setFirstName ($array['FirstName']);
            $this->setLastName  ($array['LastName']);
            $this->setEmail     ($array['Email']);
            $this->setAddress1  ($array['Address1']);
            $this->setAddress2  ($array['Address2']);
            $this->setPostalCode($array['PostalCode']);
            $this->setCity      ($array['City']);
            $this->setBirthday  ($array['Birthday']);
            $this->setPhone1    ($array['Phone1']);
            $this->setCountryId ($array['CountryId']);
    }
    
}