<?php
namespace ModernWays\FricFrac\Model;

class EventTopic {
    
    private $id;
    private $name;
    private $list;
    
    public function setName($value) {
        $this->name = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setId($value) {
        $this->id = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function toArray() {
        return array(
            "Id" => $this->getId(),
            "Name" => $this->getName());
    }
    
    public function arrayToObject($array) {
        $this->setId($array['Id']);
        $this->setName($array['Name']);
    }
    
}