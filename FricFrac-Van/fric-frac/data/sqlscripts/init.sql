

DROP TABLE IF EXISTS Person;

CREATE TABLE Person (
	Id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	FirstName NVARCHAR(50) NOT NULL,
	LastName NVARCHAR(120) NOT NULL,
	Email NVARCHAR(255),
	Address1 NVARCHAR(255),
	Address2 NVARCHAR(255),
	PostalCode NVARCHAR(20),
	City NVARCHAR(80),
	CountryId NVARCHAR(50) NOT NULL,
	Phone1 NVARCHAR(25),
	Birthday NVARCHAR(255)

);



DROP TABLE IF EXISTS Country;

CREATE TABLE Country (
	Id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	Name NVARCHAR(50) UNIQUE NOT NULL,
	Code NVARCHAR(4)
);




DROP TABLE IF EXISTS Role;

CREATE TABLE Role (
	Id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	Name NVARCHAR(50) NOT NULL
);




DROP TABLE IF EXISTS User;

CREATE TABLE User(
    Id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    Name NVARCHAR(50) NOT NULL,
    Salt NVARCHAR(255),
    HashedPassword NVARCHAR(255) NOT NULL,
    PersonId INT (11)UNSIGNED NOT NULL,
    RoleId INT (11)UNSIGNED NOT NULL
);




DROP TABLE IF EXISTS EventCategory;

CREATE TABLE EventCategory (
	Id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	Name NVARCHAR(120) NOT NULL
);



DROP TABLE IF EXISTS EventTopic;

CREATE TABLE EventTopic (
	Id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	Name NVARCHAR(120) NOT NULL
);




DROP TABLE IF EXISTS Event;

CREATE TABLE Event(
    Id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    Name NVARCHAR(120) NOT NULL,
    Location NVARCHAR(120) NOT NULL,
    Starts DATETIME,
    Ends DATETIME,
    Image NVARCHAR(255),
    Description NVARCHAR(1024),
    OrganiserName NVARCHAR(120),
    OrganiserDescription NVARCHAR(120),
    EventCategoryId INT (11)UNSIGNED NOT NULL,
    EventTopicId INT (11)UNSIGNED NOT NULL
);




ALTER TABLE Person
ADD FOREIGN KEY (CountryId) REFERENCES Country(Id);


ALTER TABLE User
ADD FOREIGN KEY (PersonId) REFERENCES Person(Id);

ALTER TABLE User
ADD FOREIGN KEY (RoleId) REFERENCES Role(Id);


ALTER TABLE Event
ADD FOREIGN KEY (EventCategoryId) REFERENCES EventCategory(Id);

ALTER TABLE Event
ADD FOREIGN KEY (EventTopicId) REFERENCES EventTopic(Id);
