<!-- Index Temp -->

<?php
     include('template/header.php');
?>

  <div class="container FirstRow">
    
      <div class="row">
          <div class="col">
               <a href="Person/Index.php" class="btn-lg btn-block btn btn-primary " role="button">Person</a>
          </div>
          <div class="col">
               <a href="Country/Index.php" class="btn-lg btn btn-primary btn-block" role="button">Land</a>
          </div>
          <div class="col-6">
               <a href="#" class="btn-lg btn-block btn btn-light disabled" role="button">.</a>
          </div>
      </div>
      
      
      <div class="row">
          <div class="col">
               <a href="#" class="btn-lg btn-block btn btn-light disabled" role="button">.</a>
          </div>
          <div class="col">
              <a href="Role/Index.php" class="btn-lg btn btn-primary btn-block" role="button">Role</a>
          </div>
          <div class="col">
               <a href="Role/create.php" class="btn-lg btn-block btn btn-primary disabled" role="button">User</a>
          </div>
          <div class="col">
                <a href="#" class="btn-lg btn-block btn btn-light disabled" role="button">.</a>
          </div>
      </div>
      
      
      <div class="row">
          <div class="col">
               <a href="Event/Index.php" class="btn-lg btn btn-primary btn-block " role="button">Event</a>
          </div>
          <div class="col">
               <a href="EventCategory/Index.php" class="btn-lg btn btn-primary btn-block" role="button">Event Category</a>
          </div>
          <div class="col">
               <a href="EventTopic/Index.php" class="btn-lg btn btn-primary btn-block" role="button">Event Topic</a>
          </div>
          <div class="col">
               <a href="#" class="btn-lg btn-block btn btn-light disabled" role="button">.</a>
          </div>
      </div>
  </div>
   
   
<!-- footer Temp --> 
<?php
     include('template/footer.php');
?>
