<!-- Person Updating One Temp -->


<?php
   include ('../template/header.php');
?>

<?php
    
    $id = $_GET['Id'];
    $model = new \ModernWays\FricFrac\Model\Person();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\Person::readOneById($id));

    $countryList = \ModernWays\FricFrac\Dal\Country::readAll();


    
    if(isset($_POST['uc'])) {
        
        $model = new \ModernWays\FricFrac\Model\Person();
        $model->setId                   ($id);
        $model->setFirstName            ($_POST['FirstName']);
        $model->setLastName             ($_POST['LastName']);
        $model->setEmail                ($_POST['Email']);
        $model->setBirthday             ($_POST['Birthday']);
        $model->setAddress1             ($_POST['Address1']);
        $model->setAddress2             ($_POST['Address2']);
        $model->setPostalCode           ($_POST['PostalCode']);
        $model->setCity                 ($_POST['City']);   
        $model->setPhone1               ($_POST['Phone1']);
        $model->setCountryId            ($_POST['CountryId']);
        \ModernWays\FricFrac\Dal\Person::update($model->toArray());
        header("Location: Index.php");
    }
?>




<div class="person container-fluid ">
    <form id="form" action="" method="POST">
        <div class="row">
           <div class="col-md-7">
                <div class="row border mb-3 bg-light rounded">
                    
                    <div class="col-md-6 ">
                        <span class="badge badge-default" style="font-size:30px;">Person</span>
                    </div>
                    
                    <div class="col-6 text-right" style="padding:4px 0;">
                       <a      class="btn btn-md btn-warning float-right" href="Index.php" role="button">Annuleren </a>
                       <button class="btn btn-md btn-success float-right" type="submit" name="uc" value="update" form="form" />update</button>
                    </div>
                    
                </div>

             
                <div class="myForm container border">
                   <form id="form" action="" method="POST">
                     <ul class="list-unstyled">
                        <li class="form-group row">
                            <label for="FirstName" class="col-3 col-form-label">FirstName</label>
                            <div class="col-9">
                                <input class="form-control" type="text" name="FirstName" id="FirstName"
                                required value="<?php echo $model->getFirstName();?>" />
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="LastName" class="col-3 col-form-label">LastName</label>
                            <div class="col-9">
                                <input class="form-control" type="text" required id="LastName" name="LastName"
                                required value="<?php echo $model->getLastName();?>" />
                            </div>
                        </li>
                        <li class="form-group row">
                            <label for="Birthday" class="col-3 col-form-label">Gebortdatum</label>
                            <div class="col-9">
                                <input class="form-control" type="date" name="Birthday" id="Birthday" 
                                required value="<?php echo $model->getBirthday();?>" />
                            </div>
                        </li>
                        <li class="form-group row">
                            <label for="Email" class="col-3 col-form-label">Email</label>
                            <div class="col-9">
                               <input class="form-control" type="email" name="Email" id="Email"
                                required value="<?php echo $model->getEmail();?>" />
                            </div>
                        </li>
                        <li class="form-group row">
                            <label for="Phone1" class="col-3 col-form-label">GSM</label>
                            <div class="col-9">
                               <input class="form-control" type="tel" name="Phone1" id="Phone1" 
                                required value="<?php echo $model->getPhone1();?>" />
                            </div>
                        </li>
                         <li class="form-group row">
                            <label for="Address1" class="col-3 col-form-label">Adres 1</label>
                            <div class="col-9">
                               <input class="form-control" type="text" name="Address1" id="Address1"
                                required value="<?php echo $model->getAddress1();?>" />
                            </div>
                        </li>

                        <li class="form-group row">
                             <label for="Address2" class="col-3 col-form-label">Adres 2</label>
                             <div class="col-9">
                                <input class="form-control" type="text"  name="Address2" id="Address2"
                                required value="<?php echo $model->getAddress2();?>" />
                            </div>
                        </li>
                        <li class="form-group row">
                            <label for="City" class="col-3 col-form-label">Stad</label>
                           <div class="col-9">
                                <input class="form-control" type="text"  name="City"  id="City"
                                required value="<?php echo $model->getCity();?>" />
                            </div>
                        </li>
                        <li class="form-group row">
                            <label for="PostalCode" class="col-3 col-form-label">PostCode</label>
                            <div class="col-9">
                                <input class="form-control" type="number" name="PostalCode" id="PostalCode"
                                required value="<?php echo $model->getPostalCode();?>" />
                            </div>
                        </li>
                         <li class="form-group row">
                            <label for="CountryId" class="col-3 col-form-label">Land</label>
                            <div class="col-9">
                                <select class="form-control" id="CountryId" name="CountryId" readonly>
                                    <!-- option elementen -->
				                    <?php
				                    if ($countryList) {
				                        foreach ($countryList as $row) {
				                    ?>
				                    	<option value="<?php echo $row['Id'];?>" <?php echo $model->getCountryId() == $row['Id'] ? 'selected' : '';?> >
				                        <?php echo $row['Name'];?>
				                    </option>
				                    <?php
				                        }
				                    }
				                    ?>   
                                </select>
                            </div>
                        </li>
                    </ul>
                </div>  
              </form>
                <div class="alert alert-success" role="alert" style="margin-top:9px" id="feedback">
                </div>
         </div>
           <div class="col-md-5">
            <?php include('ReadingAll.php');?>
        </div>
    </div>
</div>
