<!-- Person Reading ALL Temp --> 

<?php
    $Personenlist = \ModernWays\FricFrac\Dal\Person::readAll();
?>

<table class="table table-striped table-hover table-bordered">
	   <thead>
					<tr>
						<th>Select  </th>
						<th>FirstName   </th>
						<th>LastName  </th>
					</tr>
       </thead>
	   <tbody>
				 <?php
				    if ($Personenlist) {
				    foreach ($Personenlist as $PersonenItem) {
			   	 ?>
				    <tr>
    				    <td><a href="ReadingOne.php?Id=<?php echo $PersonenItem['Id'];?>">-></a></td>
                        <td><?php echo $PersonenItem['FirstName'];?></td>
                        <td><?php echo $PersonenItem['LastName'];?></td>
				    </tr>        
				<?php
				    }
				}
				?>
		</tbody>
</table>