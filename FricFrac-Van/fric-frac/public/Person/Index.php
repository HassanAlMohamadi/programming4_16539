<!-- Person Index Temp -->

<?php
    include('../template/header.php');
 ?>

<div class="person container-fluid">
	<div class="row">
		<div class="col-md-7">
			<div class="row border mb-4 bg-light rounded" >
				<div class="col-md-10">
					<span class="badge badge-default PT">Person</span>
				</div>
				<div class="col-md-2" style="padding-right: 0">
				    <a href="InsertingOne.php" class="btn btn-lg btn-primary float-right" role="button">Inserting</a>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<?php include('ReadingAll.php');?>
		</div>
	</div>
</div>

<!-- End Body -->
