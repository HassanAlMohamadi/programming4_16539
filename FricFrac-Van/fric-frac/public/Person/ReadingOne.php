<?php
    include ('../template/header.php');
?>

<?php

    $countryList = \ModernWays\FricFrac\Dal\Country::readAll();
    $id = $_GET['Id'];
    $model = new \ModernWays\FricFrac\Model\Person();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\Person::readOneById($id));

     if(isset($_POST['uc'])) {
       if ($_POST['uc'] == 'delete') {
            \ModernWays\FricFrac\Dal\Person::delete($id);
            header("Location: Index.php");
       }
    }
    
    
    if(isset($_POST['uc'])) {
        $model = new \ModernWays\FricFrac\Model\Person();
        $model->setFirstName            ($_POST['FirstName']);
        $model->setLastName             ($_POST['LastName']);
        $model->setEmail                ($_POST['Email']);
        $model->setBirthday             ($_POST['Birthday']);
        $model->setAddress1             ($_POST['Address1']);
        $model->setAddress2             ($_POST['Address2']);
        $model->setPostalCode           ($_POST['PostalCode']);
        $model->setCity                 ($_POST['City']);   
        $model->setPhone1               ($_POST['Phone1']);
        $model->setCountryId            ($_POST['CountryId']);        
        \ModernWays\FricFrac\Dal\Person::update($model->toArray());
        header("Location: Index.php");
    }
?>
    


<div class="person container-fluid ">
        <div class="row">
        
            <div class="col-md-7">
                <div class="row border mb-3 bg-light rounded" style="padding:0">
                    <div class="col-md-6 ">
                        <span class="badge badge-default" style="font-size:30px;">Person</span>
                    </div>
                    
                   <!-- Buttons Group -->
                    <div class="col-6 text-right" style="padding:4px 0;">
                        <a      class="btn btn-md btn-warning  float-right" href="Index.php" role="button">Annuleren </a>
                        <button class="btn btn-md btn-danger   float-right" type="submit" name="uc" value="delete" form="form" role="button">Delete </button>
                        <a      class="btn btn-md btn-primary  float-right" href="InsertingOne.php" role="button">Inserting</a>
                        <a      class="btn btn-md btn-success  float-right" href="UpdatingOne.php?Id=<?php echo $model->getId();?>" role="button" />Updating</a>
                    </div>
                </div>

                <div class="myForm container border">
                  <form id="form" action="" method="POST">
                    <ul class="list-unstyled">
                        <li class="form-group row">
                            <label for="FirstName" class="col-3 col-form-label">VoorNaam</label>
                            <div class="col-9">
                                <input class="form-control" type="text" name="FirstName" id="FirstName"
                                readonly value="<?php echo $model->getFirstName();?>"/>
                            </div>
                        </li>
                        <li class="form-group row">
                            <label for="LastName" class="col-3 col-form-label">FamilyNaam</label>
                            <div class="col-9">
                                <input class="form-control" type="text" required id="LastName" name="LastName" 
                                readonly value="<?php echo $model->getLastName();?>"/>
                            </div>
                        </li>
                        <li class="form-group row">
                            <label for="Birthday" class="col-3 col-form-label">GebortDatum</label>
                            <div class="col-9">
                                <input class="form-control" type="date" required id="Birthday" name="Birthday"
                                readonly value="<?php echo $model->getBirthday();?>"/>
                            </div>
                        </li>
                        <li class="form-group row">
                            <label for="Email" class="col-3 col-form-label">Email</label>
                            <div class="col-9">
                                <input class="form-control" type="email" required id="Email" name="Email"
                                readonly value="<?php echo $model->getEmail();?>"/>
                            </div>
                        </li>
                        <li class="form-group row">
                            <label for="Phone1" class="col-3 col-form-label">Phone1</label>
                            <div class="col-9">
                                <input class="form-control" type="tel" required id="Phone1" name="Phone1" 
                                readonly value="<?php echo $model->getPhone1();?>"/>
                            </div>
                        </li>
                        <li class="form-group row">
                            <label for="Address1" class="col-3 col-form-label">Adres 1</label>
                            <div class="col-9">
                                <input class="form-control" type="text" required id="Address1" name="Address1" 
                                readonly value="<?php echo $model->getAddress1();?>"/>
                            </div>
                        </li>
                        <li class="form-group row">
                            <label for="Address2" class="col-3 col-form-label">Adres 2</label>
                            <div class="col-9">
                                <input class="form-control" type="text" required id="Address2" name="Address2" 
                                readonly value="<?php echo $model->getAddress2();?>"/>
                            </div>
                        </li>
                        <li class="form-group row">
                            <label for="City" class="col-3 col-form-label">Stad</label>
                            <div class="col-9">
                                <input class="form-control" type="text" required id="City" name="City" 
                                 readonly value="<?php echo $model->getCity();?>"/>
                            </div>
                        </li>
                         <li class="form-group row">
                            <label for="PostalCode" class="col-3 col-form-label">PostalCode</label>
                            <div class="col-9">
                                <input class="form-control" type="number" required id="PostalCode" name="PostalCode" 
                                readonly value="<?php echo $model->getPostalCode();?>"/>
                            </div>
                        </li>
                        <li class="form-group row">
                            <label for="CountryId" class="col-3 col-form-label">Land</label>
                            <div class="col-9">
                                
                                <select class="form-control" id="CountryId" name="CountryId" readonly>
                                    <!-- option elementen -->
				                    <?php
				                    if ($countryList) {
				                        foreach ($countryList as $row) {
				                    ?>
				                    <option value="<?php echo $row['Id'];?>" <?php echo $model->getCountryId() == $row['Id'] ? 'selected' : '';?> >
				                        <?php echo $row['Name'];?>
				                    </option>
				                    <?php
				                        }
				                    }
				                    ?>   
                                </select>
                            </div>
                        </li>
                    </ul>
                </div>
           </form>
        </div>

         <div class="col-md-5">
            <?php include('ReadingAll.php');?>
        </div>
    </div>
</div>