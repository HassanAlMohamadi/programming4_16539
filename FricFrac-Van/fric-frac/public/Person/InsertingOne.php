<!-- Person Inserting one -->

<?php
    include ('../template/header.php');
?>


<?php
    $countryList    = \ModernWays\FricFrac\Dal\Country::readAll();
    
    if(isset($_POST['uc'])) {
        $model = new \ModernWays\FricFrac\Model\Person();
        $model->setFirstName            ($_POST['FirstName']);
        $model->setLastName             ($_POST['LastName']);
        $model->setEmail                ($_POST['Email']);
        $model->setBirthday             ($_POST['Birthday']);
        $model->setAddress1             ($_POST['Address1']);
        $model->setAddress2             ($_POST['Address2']);
        $model->setPostalCode           ($_POST['PostalCode']);
        $model->setCity                 ($_POST['City']);   
        $model->setPhone1               ($_POST['Phone1']);
        $model->setCountryId            ($_POST['CountryId']);
        \ModernWays\FricFrac\Dal\Person::create($model->toArray());
        header("Location:".$_SERVER['PHP_SELF']);
        exit;
    }
?>


<div class="person container-fluid ">
    <div class="row">
        <div class="col-md-7">
            <div class="row border mb-3 bg-light rounded">
                <div class="col-md-9 ">
                    <span class="badge badge-default" style="font-size:30px;">Person</span>
                </div>

                <div class="col-3 text-right" style="padding:4px 0;">
                    <button type="submit" name="uc" value="insert" form="form" class="btn btn-success btn-md">Insert</button>
                    <a href="Index.php" class="btn btn-danger btn-md" role="button">Annuleren</a>
                </div>
            </div>

            <div class="myForm container border">
                <form id="form" action="" method="POST">
                    <ul class="list-unstyled">
                        <li class="form-group row">
                            <label for="FirstName" class="col-2 col-form-label">Voornaam</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" name="FirstName" id="FirstName">
                            </div>
                        </li>
                        <li class="form-group row">
                            <label for="LastName" class="col-2 col-form-label">Familienaam</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" name="LastName" id="LastName">
                            </div>
                        </li>
                        <li class="form-group row">
                            <label for="Birthday" class="col-2 col-form-label">Gebortdatum</label>
                            <div class="col-10">
                                <input class="form-control" type="date" value="" name="Birthday" id="Birthday">
                            </div>
                        </li>
                        <li class="form-group row">

                            <label for="Email" class="col-2 col-form-label">Email</label>
                            <div class="col-10">
                                <input class="form-control" type="email" value="" name="Email" id="Email">
                            </div>

                        </li>
                        <li class="form-group row">
                            <label for="Phone1" class="col-2 col-form-label">GSM</label>
                            <div class="col-10">
                                <input class="form-control" type="tel" value="" name="Phone1" id="Phone1">
                            </div>
                        </li>
                        <li class="form-group row">
                            <label for="Address1" class="col-2 col-form-label">Adres 1</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" name="Address1" id="Address1">
                            </div>
                        </li>
                        <li class="form-group row">

                            <label for="Address2" class="col-2 col-form-label">Adres 2</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" name="Address2" id="Address2">
                            </div>

                        </li>
                        <li class="form-group row">
                            <label for="City" class="col-2 col-form-label">Stad</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" name="City" id="City">
                            </div>
                        </li>
                        <li class="form-group row">
                            <label for="PostalCode" class="col-2 col-form-label">PostCode</label>
                            <div class="col-10">
                                <input class="form-control" type="number" value="" name="PostalCode" id="PostalCode">
                            </div>
                        </li>
                        <li class="form-group row">
                            <label for="CountryId" class="col-2 col-form-label">Land</label>
                            <div class="col-10">
                                <select class="form-control" id="CountryId" name="CountryId">
                                    <!-- option elementen -->
                                    <?php
                                       if ($countryList) {
                                         foreach ($countryList as $row) {
                                         ?>
                                    <option value="<?php echo $row['Id'];?>">
                                        <?php echo $row['Name'];?>
                                    </option>
                                    <?php
                                                  }
                                                }
                                            ?>
                                </select>
                            </div>
                        </li>
                    </ul>
                </form>
            </div>


            <div class="alert alert-success" role="alert" style="margin-top:9px" id="feedback">
                <div class="text-center">
                    <?php
                        if (isset($_POST['uc']) && $model) {
                            echo '<h3 class="alert-heading">Well done!</h3>'."Peron {$PersonenItem['FirstName']} {$PersonenItem['LastName']} is toegevoegd.";
                        }
                        ?>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <?php include('ReadingAll.php');?>
        </div>

    </div>
</div>