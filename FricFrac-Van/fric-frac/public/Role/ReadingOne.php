<!-- Role Reading One Temp -->


<?php
    include ('../template/header.php');
    $id = $_GET['Id'];
    $model = new \ModernWays\FricFrac\Model\Role();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\Role::readOneById($id));

   if(isset($_POST['uc'])) {
       if ($_POST['uc'] == 'delete') {
            \ModernWays\FricFrac\Dal\Role::delete($id);
            header("Location: Index.php");
       }
    }    
?>


<div class="person container-fluid ">
    <div class="row">
        <div class="col-md-7">
            <div class="person-row row border shadow-none mb-4 bg-light rounded" style="padding:0">
                <div class="col-6 ">
                    <span class="badge badge-default" style="font-size:20px; margin-top:6px">Role</span>
                </div>
                <!-- Buttons Group -->
                <div class="col-6 text-right" style="padding-right:0;">
                    <a      class="btn btn-md btn-warning float-right" href="Index.php" role="button">Cansel </a>
                    <button class="btn btn-md btn-danger  float-right" type="submit" name="uc" value="delete" form="form" role="button">Delete </button>
                    <a      class="btn btn-md btn-primary float-right" href="InsertingOne.php" role="button">Inserting</a>
                    <a      class="btn btn-md btn-success float-right" href="UpdatingOne.php?Id=<?php echo $model->getId();?>" role="button" />Updating</a>
                </div>
            </div>
            
            <div class="myForm container border">
              <form id="form" action="" method="POST">
                <div class="form-group row">
                    <label for="Name" class="col-3 col-form-label">EventTopic Naam</label>
                    <div class="col-9">
                        <input class="form-control" type="text" name="Name" id="Name" readonly value="<?php echo $model->getName();?>" />
                    </div>
                </div>
               </form>
             </div>
        </div>
        
        <div class="col-md-5">
            <?php include('ReadingAll.php');?>
        </div>
        
    </div>
</div>