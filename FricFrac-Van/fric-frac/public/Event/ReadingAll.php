<!-- Event Reading ALL Temp --> 

<?php
    $Eventlist = \ModernWays\FricFrac\Dal\Event::readAll();
?>

<table class="table table-striped table-hover table-bordered">
	   <thead>
					<tr>
						<th>Select  </th>
						<th>Event   </th>
						<th>Plaats  </th>
					</tr>
       </thead>
	   <tbody>
				 <?php
				    if ($Eventlist) {
				    foreach ($Eventlist as $EventItem) {
			   	 ?>
				    <tr>
    				    <td><a href="ReadingOne.php?Id=<?php echo $EventItem['Id'];?>">-></a></td>
                        <td><?php echo $EventItem['Name'];?></td>
                        <td><?php echo $EventItem['Location'];?></td>
				    </tr>        
				<?php
				    }
				}
				?>
		</tbody>
</table>