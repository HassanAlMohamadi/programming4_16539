<!--inserting Event Temp -->

<?php
    include ('../template/header.php');
?>


<?php
    $categoryList = \ModernWays\FricFrac\Dal\EventCategory::readAll();
    $topicList    = \ModernWays\FricFrac\Dal\EventTopic::readAll();
    
    if(isset($_POST['uc'])) {
        $model = new \ModernWays\FricFrac\Model\Event();
        $model->setName                 ($_POST['Name']);
        $model->setLocation             ($_POST['Location']);
        $model->setImage                ($_POST['Image']);
        $model->setStarts               ($_POST['StartDate'] . ' ' .$_POST['StartTime']);
        $model->setEnds                 ($_POST['EndDate']    . ' ' .$_POST['EndTime']);
        $model->setDescription          ($_POST['Description']);
        $model->setOrganiserName        ($_POST['OrganiserName']);
        $model->setOrganiserDescription ($_POST['OrganiserDescription']);
        $model->setEventCategoryId      ($_POST['EventCategoryId']);
        $model->setEventTopicId         ($_POST['EventTopicId']);        
        \ModernWays\FricFrac\Dal\Event::create($model->toArray());
        header("Location:".$_SERVER['PHP_SELF']);
        exit;
    }
?>


<div class="person container-fluid ">
    <form id="form" action="" method="POST">
        <div class="row">
            <div class="col-md-7">
                <div class="row border mb-3 bg-light rounded" >
                    <div class="col-md-9 ">
                        <span class="badge badge-default" style="font-size:30px;">Event</span>
                    </div>
                    
                    <div class="col-3 text-right" style="padding:4px 0;">
                        <button type="submit" name="uc" class="btn btn-success btn-md">Insert</button>
                        <a href="Index.php" class="btn btn-danger btn-md" role="button">Annuleren</a>
                    </div>
                </div>


                <div class="myForm container border">
                    <ul class="list-unstyled">
                        <li class="form-group row">
                            <label for="Name" class="col-3 col-form-label">Naam</label>
                            <div class="col-9">
                                <input class="form-control" type="text" value="" name="Name" id="Name">
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="Location" class="col-3 col-form-label">Plaats</label>
                            <div class="col-9">
                                <input class="form-control" type="text" required id="Location" name="Location">
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="StartTime" class="col-3 col-form-label">Starttijd</label>
                            <div class="col-9">
                                <input class="form-control" type="time" required id="StartTime" name="StartTime">
                            </div>
                        </li>
                        
                         <li class="form-group row">
                            <label for="EndTime" class="col-3 col-form-label">EndTijd</label>
                            <div class="col-9">
                                <input class="form-control" type="time" required id="EndTime" name="EndTime" />
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="StartDate" class="col-3 col-form-label">Startdatum</label>
                            <div class="col-9">
                                <input class="form-control" type="date" required id="StartDate" name="StartDate">
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="EndDate" class="col-3 col-form-label">EndDatum</label>
                            <div class="col-9">
                                <input class="form-control" type="date" required id="EndDate" name="EndDate" />
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="Image" class="col-3 col-form-label">Afbeelding</label>
                            <div class="col-9">
                                <input class="form-control" type="text" required id="Image" name="Image" />
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="Description" class="col-3 col-form-label">Beschrijving</label>
                            <div class="col-9">
                                <input class="form-control" type="text" required id="Description" name="Description" />
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="OrganiserName" class="col-3 col-form-label">Naam organisator</label>
                            <div class="col-9">
                                <input class="form-control" type="text" required id="OrganiserName" name="OrganiserName" />
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="OrganiserDescription" class="col-3 col-form-label">Beschrijving organisator</label>
                            <div class="col-9">
                                <input class="form-control" type="text" required id="OrganiserDescription" name="OrganiserDescription" />
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="EventCategoryId" class="col-3 col-form-label">Event categorie</label>
                            <div class="col-9">
                                <select class="form-control" id="EventCategoryId" name="EventCategoryId">
                                    <!-- option elementen -->
                                    <?php
                                            if ($categoryList) {
                                                foreach ($categoryList as $row) {
                                            ?>
                                    <option value="<?php echo $row['Id'];?>">
                                        <?php echo $row['Name'];?>
                                    </option>
                                    <?php
                                                }
                                            }
                                            ?>
                                </select>
                            </div>
                        </li>

                        <li class="form-group row">
                            <label class="col-3 col-form-label" for="EventTopicId">Event topic</label>
                            <div class="col-9">
                                <select class="form-control" id="EventTopicId" name="EventTopicId">
                                    <!-- option elementen -->
                                    <?php
                                            if ($topicList) {
                                                foreach ($topicList as $row) {
                                            ?>
                                    <option value="<?php echo $row['Id'];?>">
                                        <?php echo $row['Name'];?>
                                    </option>
                                    <?php
                                                }
                                              }
                                            ?>
                                </select>
                            </div>
                        </li>
                    </ul>
                </div>
          </form>

        <div class="alert alert-success" role="alert" style="margin-top:9px" id="feedback">
            <div class="text-center">
                <?php
                    if (isset($_POST['uc']) && $model) {
                        echo '<h3 class="alert-heading">Well done!</h3>'."Event {$EventList['Name']} is toegevoegd.";
                    }
                    ?>
            </div>
        </div>

</div>

<div class="col-md-5">
    <?php include('ReadingAll.php');?>
</div>