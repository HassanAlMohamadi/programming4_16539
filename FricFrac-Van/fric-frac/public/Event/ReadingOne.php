<!-- Event Reading One -->

<?php
    include ('../template/header.php');

?>

<?php

    $categoryList = \ModernWays\FricFrac\Dal\EventCategory::readAll();
    $topicList = \ModernWays\FricFrac\Dal\EventTopic::readAll();
    $id = $_GET['Id'];
    
    $model = new \ModernWays\FricFrac\Model\Event();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\Event::readOneById($id));
    
   if(isset($_POST['uc'])) {
       if ($_POST['uc'] == 'delete') {
            \ModernWays\FricFrac\Dal\Event::delete($id);
            header("Location: Index.php");
       }
    }
    
   /* if(isset($_POST['uc'])) {
        
        $model = new \ModernWays\FricFrac\Model\Event();
        $model->setId($_POST['Id']);
        $model->setName($_POST['Name']);
        $model->setLocation($_POST['Location']);
        $model->setImage($_POST['Image']);
        $model->setStarts($_POST['StartDate'] . ' ' .$_POST['StartTime']);
        $model->setEnds($_POST['EndDate'] . ' ' .$_POST['EndTime']);
        $model->setDescription($_POST['Description']);
        $model->setOrganiserName($_POST['OrganiserName']);
        $model->setOrganiserDescription($_POST['OrganiserDescription']);
        $model->setEventCategoryId($_POST['EventCategoryId']);
        $model->setEventTopicId($_POST['EventTopicId']);        
        \ModernWays\FricFrac\Dal\Event::update($model->toArray());
        header("Location: Index.php");
    }*/
?>
    


<div class="person container-fluid ">
    <form id="form" action="" method="POST">

        <div class="row">

            <div class="col-md-7">
                <div class="row border mb-3 bg-light rounded" style="padding:0">
                    <div class="col-md-6 ">
                        <span class="badge badge-default" style="font-size:30px;">Event</span>
                    </div>
                    
                <!-- Buttons Group -->

                <div class="col-6 text-right" style="padding:4px 0;">
                    <a      class="btn btn-md btn-warning  float-right" href="Index.php" role="button">Annuleren </a>
                    <button class="btn btn-md btn-danger   float-right" type="submit" name="uc" value="delete" form="form" role="button">Delete </button>
                    <a      class="btn btn-md btn-primary  float-right" href="InsertingOne.php" role="button">Inserting</a>
                    <a      class="btn btn-md btn-success  float-right" href="UpdatingOne.php?Id=<?php echo $model->getId();?>" role="button" />Updating</a>

                </div>

                <!-- End Buttons Group -->

                </div>


                <div class="myForm container border">
                    <ul class="list-unstyled">

                        <li class="form-group row">
                            <label for="Name" class="col-3 col-form-label">Naam</label>
                            <div class="col-9">
                                <input class="form-control" type="text" name="Name" id="Name"
                                readonly value="<?php echo $model->getName();?>"/>
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="Location" class="col-3 col-form-label">Plaats</label>
                            <div class="col-9">
                                <input class="form-control" type="text" required id="Location" name="Location" readonly value="<?php echo $model->getLocation();?>"/>
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="StartTime" class="col-3 col-form-label">Starttijd</label>
                            <div class="col-9">
                                <input class="form-control" type="time" required id="StartTime" name="StartTime" readonly value="<?php echo $model->getStartTime();?>"/>
                            </div>
                        </li>
                        
                         <li class="form-group row">
                            <label for="EndTime" class="col-3 col-form-label">EndTijd</label>
                            <div class="col-9">
                                <input class="form-control" type="time" required id="EndTime" name="EndTime"
                                readonly value="<?php echo $model->getEndTime();?>"/>
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="StartDate" class="col-3 col-form-label">Startdatum</label>
                            <div class="col-9">
                                <input class="form-control" type="date" required id="StartDate" name="StartDate"
                                readonly value="<?php echo $model->getStartDate();?>"/>
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="EndDate" class="col-3 col-form-label">EndDatum</label>
                            <div class="col-9">
                                <input class="form-control" type="date" required id="EndDate" name="EndDate" 
                                readonly value="<?php echo $model->getEndDate();?>"/>
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="Image" class="col-3 col-form-label">Afbeelding</label>
                            <div class="col-9">
                                <input class="form-control" type="text" required id="Image" name="Image"
                                readonly value="<?php echo $model->getImage();?>"/>
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="Description" class="col-3 col-form-label">Beschrijving</label>
                            <div class="col-9">
                                <input class="form-control" type="text" required id="Description" name="Description" 
                                 readonly value="<?php echo $model->getDescription();?>"/>
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="OrganiserName" class="col-3 col-form-label">Naam organisator</label>
                            <div class="col-9">
                                <input class="form-control" type="text" required id="OrganiserName" name="OrganiserName" readonly value="<?php echo $model->getOrganiserName();?>"/>
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="OrganiserDescription" class="col-3 col-form-label">Beschrijving organisator</label>
                            <div class="col-9">
                                <input class="form-control" type="text" required id="OrganiserDescription" name="OrganiserDescription" readonly value="<?php echo $model->getOrganiserDescription();?>"/>
                            </div>
                        </li>

                        <li class="form-group row">
                            <label for="EventCategoryId" class="col-3 col-form-label">Event categorie</label>
                            <div class="col-9">
                                <select class="form-control" id="EventCategoryId" name="EventCategoryId" readonly>
                                    <!-- option elementen -->
				                    <?php
				                    if ($categoryList) {
				                        foreach ($categoryList as $row) {
				                    ?>
				                        <option value="<?php echo $row['Id'];?>" <?php echo $model->getEventCategoryId() === $row['Id'] ? 'SELECTED' : '';?>>
				                        <?php echo $row['Name'];?>
				                    </option>
				                    <?php
				                        }
				                    }
				                    ?>   
                                </select>
                            </div>
                        </li>

                        <li class="form-group row">
                            <label class="col-3 col-form-label" for="EventTopicId">Event topic</label>
                            <div class="col-9">
                                <select class="form-control" id="EventTopicId" name="EventTopicId" readonly>
                                    <!-- option elementen -->
					                    <?php
					                    if ($topicList) {
					                        foreach ($topicList as $row) {
					                    ?>
					                        <option value="<?php echo $row['Id'];?>" <?php echo $model->getEventTopicId() === $row['Id'] ? 'SELECTED' : '';?>>
					                        <?php echo $row['Name'];?>
					                    </option>
					                    <?php
					                        }
					                    }
					                    ?>
                                </select>
                            </div>
                        </li>
                    </ul>
                </div>
          </form>
</div>

<div class="col-md-5">
    <?php include('ReadingAll.php');?>
</div>