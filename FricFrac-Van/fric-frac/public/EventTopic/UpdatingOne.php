<!-- EventTopic UpdateOne Temp -->


<?php
    include ('../template/header.php');
?>

<?php
    $id = $_GET['Id'];
    $model = new \ModernWays\FricFrac\Model\EventTopic();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\EventTopic::readOneById($id));
    
   if(isset($_POST['uc'])) {
        $model->setName($_POST['Name']);
        \ModernWays\FricFrac\Dal\EventTopic::update($model->toArray());
}?>



<div class="person container-fluid ">
    <div class="row">
            <div class="col-md-7">
    
                <div class="person-row row border shadow-none mb-4 bg-light rounded" style="padding:0">
                <div class="col-6 ">
                    <span class="badge badge-default" style="font-size:20px; margin-top:6px">EventTopic</span>
                </div>
    
                <!-- Buttons Group -->
                <div class="col-6 text-right" style="padding-right:0;">
                    <a class="btn btn-md btn-warning float-right" href="Index.php" role="button">Annuleren </a>
                    <button class="btn btn-md btn-success float-right" type="submit" name="uc" value="update" form="form" />Update</button>
                </div>
            </div>

            <!-- Form Group -->
            <form id="form" action="" method="POST">
                <div class="myForm container border">
                    <div class="form-group row">
                        <label for="Name" class="col-3 col-form-label">EventTopic Naam</label>
                        <div class="col-9">
                            <input class="form-control" type="text" name="Name" id="Name" required value="<?php echo $model->getName();?>" />
                        </div>
                    </div>
                </div>
            </form>

            <!-- Feed Back  -->
            <div class="alert alert-success" role="alert" style="margin-top:10px" id="feedback">
                <div class="text-center">
                    <?php
                        if (isset($_POST['submit']) && $statement) {
                            echo '<h4 class="alert-heading">Well done!</h4>'."{$EventTopic['Name']} is Updated.";
                        }
                        ?>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <?php include('ReadingAll.php');?>
        </div>

    </div>