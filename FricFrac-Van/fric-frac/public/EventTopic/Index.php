<!-- EventTopic Index Temp -->

<?php
    include('../template/header.php');
 ?>


<div class="person container-fluid">
	<div class="row">
		<div class="col-md-7">
			<div class="person-row row border shadow-none mb-4 bg-light rounded" style="padding:0">
				<div class="col-md-10">
					<span class="badge badge-default" style="font-size:20px; margin-top:6px">EventTopic</span>
				</div>
				<div class="col-md-2" style="padding-right: 0">
				    <a href="InsertingOne.php" class="btn btn-lg btn-primary float-right" role="button">Inserting</a>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<?php include('ReadingAll.php');?>
		</div>
	</div>
</div>
