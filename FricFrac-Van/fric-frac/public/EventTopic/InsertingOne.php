<!-- EventTopic Inserting One Temp -->

<?php
    include('../template/header.php');
 ?>

<?php
    if(isset($_POST['uc'])) {
        $model = new \ModernWays\FricFrac\Model\EventTopic();
        $model->setName($_POST['Name']);
        \ModernWays\FricFrac\Dal\EventTopic::create($model->toArray());
        header("Location:".$_SERVER['PHP_SELF']);
        exit;
    }
?>


<div class="person container-fluid ">
        <form id="form" action="" method="POST">
            <div class="row">
                <div class="col-md-7">
                    <div class="person-row row border shadow-none mb-4 bg-light rounded" style="padding:0">
                        <div class="col-md-8 ">
                            <span class="badge badge-default" style="font-size:20px; margin-top:6px">EventTopic</span>
                        </div>
                        <div class="col-4 text-right" style="padding-right:0;">
                            <button type="submit" name="uc" class="btn btn-success btn-lg">Insert</button>
                            <a href="Index.php" class="btn btn-danger btn-lg" role="button">Annuleren</a>
                        </div>
                    </div>
    
                    <div class="myForm container border">
                        <ul class="list-unstyled">
                            <li>
                                <div class="form-group row">
                                    <label for="Name" class="col-2 col-form-label">Naam</label>
                                    <div class="col-10">
                                        <input class="form-control" type="text" value="" name="Name" id="Name">
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
         </form>
    
        <div class="alert alert-success" role="alert" style="margin-top:10px" id="feedback">
            <div class="text-center">
                <?php
                    if (isset($_POST['uc']) && $model) {
                        echo '<h4 class="alert-heading">Well done!</h4>'."EventTopic {$updateEventTopic['Name']} is toegevoegd.";
                    }
                    ?>
            </div>
        </div>
</div>

<div class="col-md-5">
    <?php include('ReadingAll.php');?>
</div>
