<!-- EventCategory Reading One Temp -->


<?php
    include ('../template/header.php');
    $id = $_GET['Id'];
    //echo $id;
    $model = new \ModernWays\FricFrac\Model\EventCategory();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\EventCategory::readOneById($id));

   if(isset($_POST['uc'])) {
       if ($_POST['uc'] == 'delete') {
            \ModernWays\FricFrac\Dal\EventCategory::delete($id);
            header("Location: Index.php");
       }
    }    
?>




<div class="person container-fluid ">
    <form id="form" action="" method="POST">
     <div class="row">
        <div class="col-md-7">
            <div class="person-row row border shadow-none mb-4 bg-light rounded" style="padding:0">
                <div class="col-6 ">
                    <span class="badge badge-default" style="font-size:20px; margin-top:6px">EventCategory</span>
                </div>
                
                <!-- Buttons Group -->
                <div class="col-6 text-right" style="padding-right:0;">
                    <a      class="btn btn-md btn-warning float-right" href="Index.php" role="button">Cansel </a>
                    <button class="btn btn-md btn-danger  float-right" type="submit" name="uc" value="delete" form="form" role="button">Delete </button>
                    <a      class="btn btn-md btn-primary float-right" href="InsertingOne.php" role="button">Inserting</a>
                    <a      class="btn btn-md btn-success float-right" href="UpdatingOne.php?Id=<?php echo $model->getId();?>" role="button" />Updating</a>

                </div>
            </div>
            
                <!-- Form Group -->
                <div class="myForm container border">
                    <div class="form-group row">
                        <label for="Name" class="col-2 col-form-label">Country Name</label>
                        <div class="col-10">
                            <input class="form-control" type="text" name="Name" id="Name" readonly value="<?php echo $model->getName();?>" />
                        </div>
                    </div>
                </div>

                <!-- End Form Group -->
                
                <!-- Feed Back  -->
               <div class="alert alert-success" role="alert" style="margin-top:10px" id="feedback">
                  <div class="text-center">
                    <?php
                    if (isset($_POST['submit']) && $statement) {
                        echo '<h4 class="alert-heading">Well done!</h4>'."{$CEventCategory['Name']} is Updated.";
                    }
                    ?>
                 </div>
               </div>
          </form>
       </div>
        <div class="col-md-5">
            <?php include('ReadingAll.php');?>
        </div>
</div>