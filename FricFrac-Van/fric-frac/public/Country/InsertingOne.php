<!-- Land Insertin One Temp -->

<?php
    session_start();
    include('../template/header.php');
 ?>



<?php
    if(isset($_POST['uc'])) {
        $model = new \ModernWays\FricFrac\Model\Country();
        $model->setName($_POST['Name']);
        $model->setCode($_POST['Code']);
        \ModernWays\FricFrac\Dal\Country::create($model->toArray());
       

    }
?>


<div class="person container-fluid ">
    <form id="form" action="" method="POST">
        
        <div class="row">

            <div class="col-md-7">
                <div class="person-row row border shadow-none mb-4 bg-light rounded" style="padding:0">
                    <div class="col-md-8 ">
                        <span class="badge badge-default" style="font-size:20px; margin-top:6px">Land</span>
                    </div>
                    <div class="col-4 text-right" style="padding-right:0;">
                        <button type="submit" name="uc" value="insert" form="form" class="btn btn-success btn-lg">Insert</button>
                        <a href="Index.php" class="btn btn-danger btn-lg" role="button">Annuleren</a>
                    </div>
                </div>


                <div class="myForm container border">
                    <ul class="list-unstyled">
                        <li>
                            <div class="form-group row">
                                <label for="Name" class="col-2 col-form-label">Naam</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="" name="Name" id="Name">
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="form-group row">
                                <label for="Code" class="col-2 col-form-label">Code</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="" name="Code" id="Code">
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
    </form>
    
    <div class="alert alert-success" role="alert" style="margin-top:10px" id="feedback">
        <div class="text-center">
            <?php
                if (isset($_POST['uc'])) {
                    $message = \ModernWays\FricFrac\Dal\Country::getMessage();
                   
                    echo '<h4 class="alert-heading">Well done!</h4>'.$message;
                }
                ?>
        </div>
    </div>
    
</div>

<div class="col-md-5">
    <?php include('ReadingAll.php');?>

</div>
