<!-- Land Reading All Temp -->

<?php
    $countryList = \ModernWays\FricFrac\Dal\Country::readAll();
?>


<table class="table table-striped table-hover table-bordered">
	   <thead>
					<tr>
						<th>Select  </th>
						<th>Country </th>
						<th>Code    </th>
					</tr>
       </thead>
	   <tbody>
				 <?php
				    if ($countryList) {
				    foreach ($countryList as $countryItem) {
			   	 ?>
				    <tr>
				   <td><a href="ReadingOne.php?Id=<?php echo $countryItem['Id'];?>">-></a></td>
                    <td><?php echo $countryItem['Name'];?></td>
                    <td><?php echo $countryItem['Code'];?></td>
				    </tr>        
				<?php
				    }
				}
				?>
		</tbody>
</table>