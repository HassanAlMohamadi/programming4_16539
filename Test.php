<?php
/**
 * Created by PhpStorm.
 * User: xXx
 * Date: 29/11/2018
 * Time: 10:55
 */

lerenWerkenMetEcho();


function lerenWerkenMetEcho(){
    /*
    *Echo
    *Enkel Voudiges tekst
    *string interpolatie
    */

    echo 'Leren Programmeren Met Php';
    echo '<br>';
    echo ' Eerste Les';
    echo '<br>';


    $voornaam   = 'Mohammed';
    $familynaam = 'El Farsi';
    echo "Je volledige Naam is  : $voornaam $familynaam";

    // Nu Met Html En Css
    echo "<p> Je volledige Naam is  : <span style=\"color : red;\">$voornaam </span> <span style=\"color : green;\">$familynaam </span></p>";

}
