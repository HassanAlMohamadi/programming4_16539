<?php

// oefeningen over arays :

$functions = array();

for ($i = 1; $i < 20; $i++) {
    $add = 'array' . $i;
    array_push($functions, $add);
}

function array1() {
    $color = array('white', 'green', 'red', 'blue', 'black');
    echo "The memory of that scene for me is like a frame of film forever frozen at that moment: 
    the $color[2] carpet, the $color[1] lawn, the $color[0] house, the leaden sky. 
    The new president and his first lady. - Richard M. Nixon"."<br />";
}

function array2() {
    $color = array('white', 'green', 'red');
    foreach ($color as $c)
    {
        echo "$c, ";
    }
    sort($color);
    echo "<ul>";
    foreach ($color as $y)
    {
        echo "<li>$y</li>";
    }
    echo "</ul>";
}

function array3() {
    $ceu = array( "Italy"=>"Rome", "Luxembourg"=>"Luxembourg",
        "Belgium"=> "Brussels", "Denmark"=>"Copenhagen",
        "Finland"=>"Helsinki", "France" => "Paris",
        "Slovakia"=>"Bratislava", "Slovenia"=>"Ljubljana",
        "Germany" => "Berlin", "Greece" => "Athens",
        "Ireland"=>"Dublin", "Netherlands"=>"Amsterdam",
        "Portugal"=>"Lisbon", "Spain"=>"Madrid",
        "Sweden"=>"Stockholm", "United Kingdom"=>"London",
        "Cyprus"=>"Nicosia", "Lithuania"=>"Vilnius",
        "Czech Republic"=>"Prague", "Estonia"=>"Tallin",
        "Hungary"=>"Budapest", "Latvia"=>"Riga","Malta"=>"Valetta",
        "Austria" => "Vienna", "Poland"=>"Warsaw") ;
    asort($ceu) ;
    foreach($ceu as $country => $capital)
    {
        echo "The capital of $country is $capital"."<br />" ;
    }
}

function array4() {
    $x = array(1, 2, 3, 4, 5);
    var_dump($x);
    unset($x[3]);
    $x = array_values($x);
    echo '';
    var_dump($x);
}

function array5() {
    $color = array(4 => 'white', 6 => 'green', 11=> 'red');
    echo reset($color)."\n";
}

function array6() {
    function w3rfunction($value,$key){
        echo "$key : $value"."<br>";
    }
    $a = '{"Title": "The Cuckoos Calling",
            "Author": "Robert Galbraith",
            "Detail":
        { 
           "Publisher": "Little Brown"
        }
    }';
    $jasonDecode = json_decode($a,true);
    array_walk_recursive($jasonDecode,"w3rfunction");
}

function array7() {
    $original = array( '1','2','3','4','5' );
    echo 'Original array : '."<br />";
    foreach ($original as $x)
    {echo "$x ";}
    $inserted = '$';
    array_splice( $original, 3, 0, $inserted );
    echo " <br /> After inserting '$' the array is : "."<br />";
    foreach ($original as $x)
    {echo "$x ";}
    echo "<br />";
}

function array8() {
    echo "Associative array : Ascending order sort by value <br />";
    $people=array("Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40");
    asort($people);
    foreach($people as $y=>$y_value)
    {
        echo "Age of ".$y." is : ".$y_value."<br />";
    }
    echo "Associative array : Ascending order sort by Key <br />";
    $key1=array("Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40");
    ksort($key1);
    foreach($key1 as $y=>$y_value)
    {
        echo "Age of ".$y." is : ".$y_value."<br />";
    }
    echo "Associative array : Descending order sorting by Value <br />";
    $age=array("Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40");
    arsort($age);
    foreach($age as $y=>$y_value)
    {
        echo "Age of ".$y." is : ".$y_value."<br />";
    }
    echo "Associative array : Descending order sorting by Key <br />";
    $key2=array("Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40");
    krsort($key2);
    foreach($key2 as $y=>$y_value)
    {
        echo "Age of ".$y." is : ".$y_value."<br />";
    }
}

function array9() {
    $month_temp = "78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 81, 76, 73,
                    68, 72, 73, 75, 65, 74, 63, 67, 65, 64, 68, 73, 75, 79, 73";
    $temp_array = explode(',', $month_temp);
    $tot_temp = 0;
    $temp_array_length = count($temp_array);
    foreach($temp_array as $temp)
    {
        $tot_temp += $temp;
    }
    $average_high_temp = $tot_temp/$temp_array_length;
    echo "Average Temperature is : ".$average_high_temp."<br />";
    sort($temp_array);
    echo " List of five lowest temperatures : <br />";
    for ($i=0; $i< 5; $i++)
    {
        echo $temp_array[$i].", ";
    }
    echo "<br /> List of five highest temperatures : <br />";
    for ($i=($temp_array_length-5); $i< ($temp_array_length); $i++)
    {
        echo $temp_array[$i].", ";
    }
}

function array10() {
    function columns($uarr)
    {
        $n = $uarr;
        if (count($n) == 0)
            return array();
        else if (count($n) == 1)
            return array_chunk($n[0], 1);
        array_unshift($uarr, NULL);
        $transpose = call_user_func_array('array_map', $uarr);
        return array_map('array_filter', $transpose);
    }
    function bead_sort($uarr)
    {
        foreach ($uarr as $e)
            $poles []= array_fill(0, $e, 1);
        return array_map('count', columns(columns($poles)));
    }
    echo '<br /> Original Array : '.'<br />';
    print_r(array(5,3,1,3,8,7,4,1,1,3));
    echo '<br />'.' <br /> After Bead sort : '.'<br />';
    print_r(bead_sort(array(5,3,1,3,8,7,4,1,1,3)));
}

function array11() {
    $array1 = array(array(77, 87), array(23, 45));
    $array2 = array("w3resource", "com");
    function merge_arrays_by_index($x, $y)
    {
        $temp = array(); $temp[] = $x; if(is_scalar($y))
    {
        $temp[] = $y;
    }
    else
    {
        foreach($y as $k => $v)
        {
            $temp[] = $v;
        }
    }
        return $temp;
    }
    echo '<pre>'; print_r(array_map('merge_arrays_by_index',$array2, $array1));
}

function array12() {
    function array_change_value_case($input, $ucase)
    {
        $case = $ucase;
        $narray = array();
        if (!is_array($input))
        {
            return $narray;
        }
        foreach ($input as $key => $value)
        {
            if (is_array($value))
            {
                $narray[$key] = array_change_value_case($value, $case);
                continue;
            }
            $narray[$key] = ($case == CASE_UPPER ? strtoupper($value) : strtolower($value));
        }
        return $narray;
    }
    $Color = array('A' => 'Blue', 'B' => 'Green', 'c' => 'Red');
    echo 'Actual array ';
    print_r($Color);
    echo 'Values are in lower case.';
    $myColor = array_change_value_case($Color,CASE_LOWER);
    print_r($myColor);
    echo 'Values are in upper case.';
    $myColor = array_change_value_case($Color,CASE_UPPER);
    print_r($myColor);
}

function array13() {
    echo implode(",",range(200,250,4))."<br />";
}

function array14() {
    $my_array = array("abcd","abc","de","hjjj","g","wer");
    $new_array = array_map('strlen', $my_array);
    echo "The shortest array length is " . min($new_array) .
        ". The longest array length is " . max($new_array).'.';
}

function array15() {
    $r=range(11,20);
    shuffle($r);
    for ($x = 0; $x < 10; $x++)
    {
        echo $r[$x].' ';
    }
    echo "<br />";
}

function array16() {
    $ceu = array( "Italy"=>"Rome", "Luxembourg"=>"Luxembourg", "Belgium"=> "Brussels",
        "Denmark"=>"Copenhagen", "Finland"=>"Helsinki", "France" => "Paris", "Slovakia"=>"Bratislava",
        "Slovenia"=>"Ljubljana", "Germany" => "Berlin", "Greece" => "Athens", "Ireland"=>"Dublin",
        "Netherlands"=>"Amsterdam", "Portugal"=>"Lisbon", "Spain"=>"Madrid", "Sweden"=>"Stockholm",
        "United Kingdom"=>"London", "Cyprus"=>"Nicosia", "Lithuania"=>"Vilnius", "Czech Republic"=>"Prague",
        "Estonia"=>"Tallin", "Hungary"=>"Budapest", "Latvia"=>"Riga", "Malta"=> "Valetta",
        "Austria" => "Vienna", "Poland"=>"Warsaw") ;
    $max_key = max( array_keys( $ceu) );
    echo $max_key."<br />";
}

function array17() {
    function min_values_not_zero(Array $values)
    {
        return min(array_diff(array_map('intval', $values), array(0)));
    }
    print_r(min_values_not_zero(array(-5,0,15,35,-88,8))."<br />");
}

function array18() {
    function floorDec($number, $precision, $separator)
    {
        $number_part=explode($separator, $number);
        $number_part[1]=substr_replace($number_part[1],$separator,$precision,0);
        if($number_part[0]>=0)
        {$number_part[1]=floor($number_part[1]);}
        else
        {$number_part[1]=ceil($number_part[1]);}

        $ceil_number= array($number_part[0],$number_part[1]);
        return implode($separator,$ceil_number);
    }
    print_r(floorDec(1.155, 2, ".")."<br />");
    print_r(floorDec(100.25781, 4, ".")."<br />");
    print_r(floorDec(-2.9636, 3, ".")."<br />");
}

function array19() {
    $color = array ( "color" => array ( "a" => "Red", "b" => "Green", "c" => "White"),
        "numbers" => array ( 1, 2, 3, 4, 5, 6 ),
        "holes" => array ( "First", 5 => "Second", "Third"));
    echo $color["holes"][5]."<br />";
    echo $color["color"]["a"]."<br />";
}





/////////////////////////////////////////
//                                     //
//   oefeningen 2 over for loops.      //
//                                     //
/////////////////////////////////////////



for ($i = 1; $i < 11; $i++) {
    $add = 'forLoop' . $i;
    array_push($functions, $add);
}

function forLoop1() {
    for($x = 1; $x <= 10; $x++)
    {
        if($x < 10)
        {
            echo "$x-";
        }
        else
        {
            echo "$x"."<br />";
        }
    }
}

function forLoop2() {
    $sum = 0;
    for($x = 1; $x <=30 ; $x++)
    {
        $sum += $x;
    }
    echo "The sum of the numbers 0 to 30 is $sum"."<br />";
}

function forLoop3() {
    for($x = 1; $x <= 5; $x++)
    {
        for ($y = 1; $y <= $x; $y++)
        {
            echo "*";
            if($y < $x)
            {
                echo " ";
            }
        }
        echo "<br />";
    }
}

function forLoop4() {
    $n=5;
    for($x=1; $x <= $n; $x++)
    {
        for($y=1; $y <= $x; $y++)
        {
            echo ' * ';
        }
        echo '<br />';
    }
    for($x = $n; $x >= 1; $x--)
    {
        for($y = 1; $y <= $x; $y++)
        {
            echo ' * ';
        }
        echo '<br />';
    }
}

function forLoop5() {
    $n = 4;
    $x = 1;
    for($i = 1; $i <= $n-1; $i++)
    {
        $x*=($i + 1);
    }
    echo "The factorial of  $n = $x"."<br />";
}

function forLoop6() {
    for($x = 0; $x < 10; $x++)
    {
        for($y = 0; $y < 10; $y++)
        {
            echo $x.$y.", ";
        }
    }
    printf("<br />");
}

function forLoop7() {
    $text= "w3resource";
    $search_char= "r";
    $count= "0";
    for($x = "0"; $x < strlen($text); $x++)
    {
        if(substr($text,$x,1)==$search_char)
        {
            $count = $count+1;
        }
    }
    echo $count."<br />";
}

function forLoop8() {
    echo '<table align="left" border="1" cellpadding="3" cellspacing="0">';
    for($i = 1; $i <= 6; $i++)
    {
        echo '<tr>';
        for ($j = 1; $j <= 5; $j++)
        {
            echo '<td>';
            echo $i . "*" . $j . "=" . $i*$j;
            echo '</td>';
        }
        echo '</tr>';
    }
    echo '</table>';
    echo '<br /><br /><br /><br /><br /><br /><br /><br /><br />';
}

function forLoop9() {
    echo '<table width="270px" cellspacing="0px" cellpadding="0px" border = "1px">';
    for($row = 1; $row <= 8; $row++)
    {
        echo '<tr>';
        for($col = 1; $col <= 8; $col++)
        {
            $total = $row + $col;
            if($total%2==0)
            {
                echo '<td height="30px" width="30px" bgcolor="#FFFFFF">';
                echo '</td>';
            }
            else
            {
                echo '<td height="30px" width="30px" bgcolor="#000000">';
                echo '</td>';
            }
        }
        echo '</tr>';
    }
    echo '</table>';
}

function forLoop10() {
    echo '<table border ="1" style="border-collapse: collapse">';
    for ($row=1; $row <= 10; $row++) {
        echo '<tr>';
        for ($col=1; $col <= 10; $col++) {
            echo '<td>';
            echo $col * $row;
            echo '</td>';
        }
        echo '</tr>';
    }
    echo '</table>';
}



//////////////////////////////////////
//                                  //
//     Oefeningen over functions()  //
//                                  //
//////////////////////////////////////



for ($i = 1; $i < 7; $i++) {
    $add = 'function' . $i;
    array_push($functions, $add);
}

function function1() {
    function factorial_of_a_number($n)
    {
        if($n ==0)
        {
            return 1;
        }
        else
        {
            return $n * factorial_of_a_number($n-1);
        }
    }
    print_r(factorial_of_a_number(4)."<br />");
}

function function2() {
    function IsPrime($n)
    {
        for($x = 2; $x < $n; $x++)
        {
            if($n %$x ==0)
            {
                return 0;
            }
        }
        return 1;
    }
    $a = IsPrime(3);
    if ($a==0)
        echo 'This is not a Prime Number.....'."<br />";
    else
        echo 'This is a Prime Number..'."<br />";
}

function function3() {
    function reverse_string($str1)
    {
        $n = strlen($str1);
        if($n == 1)
        {
            return $str1;
        }
        else
        {
            $n--;
            return reverse_string(substr($str1,1, $n)) . substr($str1, 0, 1);
        }
    }
    print_r(reverse_string('1234')."<br />");
}

function function4() {
    function array_sort($a)
    {
        for($x = 0; $x < count($a); ++$x)
        {
            $min = $x;
            for($y = $x + 1; $y < count($a); ++$y)
            {
                if($a[$y] < $a[$min] )
                {
                    $temp = $a[$min];
                    $a[$min] = $a[$y];
                    $a[$y] = $temp;
                }
            }
        }
        return $a;
    }
    $a = array(51,14,1,21,'hj');
    print_r(array_sort($a));
}

function function5() {
    function is_str_lowercase($str1)
    {
        for ($sc = 0; $sc < strlen($str1); $sc++) {
            if (ord($str1[$sc]) >= ord('A') &&
                ord($str1[$sc]) <= ord('Z')) {
                return false;
            }
        }
        return true;
    }
    var_dump(is_str_lowercase('abc def ghi'));
    var_dump(is_str_lowercase('abc dEf ghi'));
}

function function6() {
    function check_palindrome($string)
    {
        if ($string == strrev($string))
            return 1;
        else
            return 0;
    }
    echo check_palindrome('hannah')."<br />";
}