<!-- main index -->

<?php
     include('template/header.php');
?>


<div class="container FirstRow">
    
  <div class="row">
    <div class="col">
      <a href="/fric-frac/public/Person/Index.php" class="btn-lg btn-block btn btn-primary " role="button">Person</a>
    </div>
    <div class="col">
    <a href="/fric-frac/public/Country/Index.php" class="btn-lg btn btn-primary btn-block" role="button">Country</a>
    </div>
    <div class="col-6">
    <a href="#" class="btn-lg btn-block btn btn-light disabled" role="button">.</a>
    </div>
  </div>
  
  
   <div class="row">
    <div class="col">
    <a href="#" class="btn-lg btn-block btn btn-light disabled" role="button">.</a>
    </div>
    <div class="col">
     <a href="/fric-frac/public/Role/Index.php" class="btn-lg btn btn-primary btn-block" role="button">Role</a>
    </div>
    <div class="col">
     <a href="/fric-frac/public/Role/create.php" class="btn-lg btn-block btn btn-primary disabled" role="button">User</a>
    </div>
    <div class="col">
    <a href="#" class="btn-lg btn-block btn btn-light disabled" role="button">.</a>
    </div>
  </div>
  
  
    <div class="row">
    <div class="col">
    <a href="/fric-frac/public/Event/Index.php" class="btn-lg btn btn-primary btn-block " role="button">Event</a>
    </div>
    <div class="col">
    <a href="/fric-frac/public/EventCategory/Index.php" class="btn-lg btn btn-primary btn-block" role="button">Event Category</a>
    </div>
    <div class="col">
    <a href="/fric-frac/public/EventTopic/Index.php" class="btn-lg btn btn-primary btn-block" role="button">Event Topic</a>
    </div>
    <div class="col">
    <a href="#" class="btn-lg btn-block btn btn-light disabled" role="button">.</a>
    </div>
  </div>
  
  
</div>
   
<?php
     include('template/footer.php');
?>