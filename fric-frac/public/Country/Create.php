<!-- Create Country -->


<?php
    include('../template/header.php');
 ?>
 
 <?php
    // alleen uit te voeren als er op de submit knop is gedrukt
    if (isset($_POST['submit'])) {
        include('../../config.php');
        include('../../common.php');
        $newCountry = array(
            'Name' => escape($_POST['Name']),
            'Code' => escape($_POST['Code'])
            
        );
        $statement = false;
        try {

            $sql = sprintf("INSERT INTO %s (%s) VALUES (:%s)", 'Country',
                   implode(', ', array_keys($newCountry)),
                   implode(', :', array_keys($newCountry)));
            
            $connection = new \PDO($host, $username, $password, $options);
            $statement = $connection->prepare($sql);
            
            $statement->execute($newCountry);
           
          
        } catch (\PDOException $exception) {
            echo $sql . '<br/>' . $exception->getMessage();
        }
        
    }
?>



<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">

<div class="person container-fluid ">
    <div class="row">

        <div class="col-md-7">
            <div class="person-row row border shadow-none mb-4 bg-light rounded" style="padding:0">
                <div class="col-md-8 ">
                    <span class="badge badge-default" style="font-size:20px; margin-top:6px">Country</span>
                </div>
                <div class="col-4 text-right" style="padding-right:0;">
                    <button type="submit" name="submit" class="btn btn-success btn-lg">Insert</button>
                    <a href="/fric-frac/public/Country/Index.php" class="btn btn-danger btn-lg" role="button">Annuleren</a>
                </div>
            </div>


            <div class="myForm container border">
                <ul class="list-unstyled">
                    <li>
                        <div class="form-group row">
                            <label for="Name" class="col-2 col-form-label">Naam</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" name="Name" id="Name">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="form-group row">
                            <label for="Code" class="col-2 col-form-label">Code</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" name="Code" id="Code">
                            </div>
                        </div>
                    </li>
               </ul>
            </div>
            
            <div class="alert alert-success" role="alert" style="margin-top:10px" id="feedback">
                <div class="text-center">
                    <?php
                if (isset($_POST['submit']) && $statement) {
                    echo '<h4 class="alert-heading">Well done!</h4>'."Land {$newCountry['Name']} En De Code {$newCountry['Code']} is toegevoegd.";
                }
                ?>
                </div>
            </div>
            
            
        </div>
    
</form>
        
        <div class="col-md-5" style='height: 100%;'>
            <table class="table table-striped table-hover table-bordered" >
                	<thead>
					<tr>
						<th>Select     </th>
						<th>Country Name   </th>
						<th>Code</th>
					</tr>
				</thead>
				<tbody>
				 <?php
				 if (isset($_POST['submit'])) {
           
            $statement = false;
            $sql = 'SELECT * FROM Country';
            $connection = new \PDO($host, $username, $password, $options);
            $statement = $connection->prepare($sql);
            $statement->execute();
            $result = $statement->fetchAll();
        } else { 
            include('../../config.php');
           include('../../common.php');
            $statement = false;
            $sql = 'SELECT * FROM Country';
            $connection = new \PDO($host, $username, $password, $options);
            $statement = $connection->prepare($sql);
            $statement->execute();
            $result = $statement->fetchAll();
                } 
				    if ($result && $statement->rowCount() > 0) {
				    foreach ($result as $row) {
				?>
				    <tr>
				    <td><a href="Update.php?Id=<?php echo escape($row['Id']);?>">-></a></td>
				    <td><?php echo escape($row['Name']);?></td>
				    <td><?php echo escape($row['Code']);?></td>
				    </tr>        
				<?php
				    }
				}
				?>
				</tbody>
            </table>
        </div>
        </div>
    </div>
</div>


<?php
     include('../template/footer.php');
?>