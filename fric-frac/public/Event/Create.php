<!--  create Event  -->
<?php
    include('../template/header.php');
 ?>

<?php

    if (isset($_POST['submit'])) {
        include('../../config.php');
        include('../../common.php');
        $NewEvent = array(
            'Name'                 => escape($_POST['Name'                 ]),
            'Location'             => escape($_POST['Location'             ]),
            'Starts'               => escape($_POST['Starts'               ]),
            'Ends'                 => escape($_POST['Ends'                 ]),
            'Image'                => escape($_POST['Image'                ]),
            'Description'          => escape($_POST['Description'          ]),
            'OrganiserName'        => escape($_POST['OrganiserName'        ]),
            'OrganiserDescription' => escape($_POST['OrganiserDescription' ])
           
        );
        $statement = false;
        try {
            $sql = sprintf("INSERT INTO %s (%s) VALUES (:%s)", 'Event',
                implode(', ', array_keys($NewEvent)),
                implode(', :', array_keys($NewEvent)));
            $connection = new \PDO($host, $username, $password, $options);
            $statement = $connection->prepare($sql);
            $statement->execute($NewEvent);
        }catch (\PDOException $exception) {
            echo $sql . '<br/>' . $exception->getMessage();
        }
    }
?>



<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">
<div class="person container-fluid ">
    <div class="row">
        
        <div class="col-md-7">
            <div class="person-row row border shadow-none mb-4 bg-light rounded" style="padding:0">
                <div class="col-md-8 ">
                    <span class="badge badge-default" style="font-size:20px; margin-top:6px">Person</span>
                </div>
                  <div class="col-4 text-right" style="padding-right:0;">
                    <button type="submit" name="submit"  class="btn btn-success btn-lg">Insert</button>
                    <a href="/fric-frac/public/Event/Index.php" class="btn btn-danger btn-lg" role="button">Annuleren</a>
                 </div>
            </div>
            
            
            <div class="myForm container border">
                <ul class="list-unstyled">
                <li>
                    <div class="form-group row">
                        <label for="Name" class="col-2 col-form-label">Naam</label>
                        <div class="col-10">
                            <input class="form-control" type="text" value="" name="Name" id="Name">
                        </div>
                      </div>
                </li>
                <li>
                <div class="form-group row">
                    <label for="Location" class="col-2 col-form-label">Location</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="" name="Location" id="Location">
                    </div>
                </div>
                </li>
                <li>
                <div class="form-group row">
                    <label for="Starts" class="col-2 col-form-label">Starts</label>
                    <div class="col-10">
                        <input class="form-control" type="date" value="" name="Starts" id="Starts">
                    </div>
                </div>
                </li>
                <li>
                <div class="form-group row">
                    <label for="Ends" class="col-2 col-form-label">Ends</label>
                    <div class="col-10">
                        <input class="form-control" type="date" value="" name="Ends" id="Ends">
                    </div>
                </div>
                </li>
                <li>
                <div class="form-group row">
                    <label for="Image" class="col-2 col-form-label">Image</label>
                    <div class="col-10">
                        <input class="form-control" type="file"  value="" name="Image"  id="Image">
                    </div>
                </div>
                </li>
                <li>
                <div class="form-group row">
                    <label for="Description" class="col-2 col-form-label">Description</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="" name="Description" id="Description">
                    </div>
                </div>
                </li>
                <li>
                <div class="form-group row">
                    <label for="OrganiserName" class="col-2 col-form-label">OrganiserName</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="" name="OrganiserName"  id="OrganiserName">
                    </div>
                </div>
                </li>
                  <li>
                <div class="form-group row">
                    <label for="OrganiserDescription" class="col-2 col-form-label">OrganiserDescription</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="" name="OrganiserDescription"  id="OrganiserDescription">
                    </div>
                </div>
                </li>
                <li>
          
            </div>
   </ul>         
            <div class="alert alert-success" role="alert" style="margin-top:10px" id="feedback">
              <div class="text-center">
                <?php
                if (isset($_POST['submit']) && $statement) {
                    echo '<h4 class="alert-heading">Well done!</h4>'."{$NewEvent['Name']} Event is toegevoegd.";
                }
                ?>
            </div>
        </div>
        </div>
</form>
        
        <div class="col-md-5" style='height: 100%;'>
            <table class="table table-striped table-hover table-bordered" id="feedback" >
            	<thead>
					<tr>
						<th>Name     </th>
						<th>Location </th>
					</tr>
				</thead>
				<tbody>
				 <?php
				    if ($result && $statement->rowCount() > 0) {
				    foreach ($result as $row) {
				 ?>
				    <tr>
				    <td><a href="Update.php?Id=<?php echo escape($row['Id']);?>">-></a></td>
				    <td><?php echo escape($row['Name']);?></td>
				    <td><?php echo escape($row['Location']);?></td>
				    </tr>        
				<?php
				    }
				}
				?>
				</tbody>
            </table>
        </div>
    </div>
</div>


<?php
     include('../template/footer.php');
?>