<!-- delete event page -->

<!-- update event page -->

<?php
    include('../template/header.php');
 ?>


<?php

if(isset($_GET['Id'])){
    include('../../config.php');
    include('../../common.php');
    $Id = escape($_GET['Id']);
    $statement = false;
    
    try {
        $sql = 'SELECT * FROM Event WHERE Id = :Id';
        $connection = new \PDO($host, $username, $password, $options);
        $statement = $connection->prepare($sql);
        $statement->bindParam(':Id', $Id);
        $statement->execute();
        $result = $statement->fetch(\PDO::FETCH_ASSOC);

    }catch (\PDOException $exception) {
        echo $sql . '<br/>' . $exception->getMessage();
    }
}

if(isset($_POST['submit'])){
    include('../../config.php');
    include('../../common.php');
    $Event = array(
            'Name'                 => escape($_POST['Name'                 ]),
            'Location'             => escape($_POST['Location'             ]),
            'Starts'               => escape($_POST['Starts'               ]),
            'Ends'                 => escape($_POST['Ends'                 ]),
            'Image'                => escape($_POST['Image'                ]),
            'Description'          => escape($_POST['Description'          ]),
            'OrganiserName'        => escape($_POST['OrganiserName'        ]),
            'OrganiserDescription' => escape($_POST['OrganiserDescription' ])
           
        );
    
    try{
       $sql = 'DELETE FROM Country WHERE Id = :Id';
               
        $connection = new \PDO($host, $username, $password, $options);
        $statement = $connection->prepare($sql);
        
        $statement -> bindParam(':Id',$person['Id']);
        $statement -> execute();
        
    }
    catch (\PDOException $exception){
        echo $sql .'<br/>' . $exception->getMessage();
    }
}

?>



<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
<input type="hidden" name="Id" id="Id" value="<?php echo escape($result['Id']); ?>">
<div class="person container-fluid ">
    <div class="row">
        <div class="col-md-7">
            <div class="person-row row border shadow-none mb-4 bg-light rounded" style="padding:0">
                <div class="col-6 ">
                    <span class="badge badge-default" style="font-size:20px; margin-top:6px">Person</span>
                </div>
                <div class="col-6 text-right" style="padding-right:0;" >
				    <a href="/fric-frac/public/index.php" class="btn btn-md btn-warning float-right" role="button">Cansel</a>
				    <a href="/fric-frac/public/Person/Create.php" class="btn btn-md btn-danger float-right" role="button">Delete</a>
                    <a href="/fric-frac/public/Event/Create.php" class="btn btn-md btn-primary float-right" role="button">Inserting</a>
                    <input type="submit" name="submit" value="DELETE" class="btn btn-md btn-success float-right" role="button"/>
                 </div>
            </div>
            
            <div class="myForm container border">
                <div class="form-group row">
                    <label for="Name" class="col-2 col-form-label">Name</label>
                    <div class="col-10">
                        <input class="form-control" type="text" name="Name" id="Name" value="<?php echo escape($result['Name']); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Location" class="col-2 col-form-label">Location</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="<?php echo escape($result['Location']); ?>" name="Location" id="Location">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Starts" class="col-2 col-form-label">Starts</label>
                    <div class="col-10">
                        <input class="form-control" type="date" value="<?php echo escape($result['Starts']); ?>" name="Starts" id="Starts">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Ends" class="col-2 col-form-label">Ends</label>
                    <div class="col-10">
                        <input class="form-control" type="date" value="<?php echo escape($result['Ends']); ?>" name="Ends" id="Ends">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Image" class="col-2 col-form-label">Image</label>
                    <div class="col-10">
                        <input class="form-control" type="file" value="<?php echo escape($result['Image']); ?>" name="Image" id="Image">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Description" class="col-2 col-form-label">Description</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="<?php echo escape($result['Description']); ?>" name="Description" id="Description">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="OrganiserName" class="col-2 col-form-label">OrganiserName</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="<?php echo escape($result['OrganiserName']); ?>" name="OrganiserName"  id="OrganiserName">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="OrganiserDescription" class="col-2 col-form-label">OrganiserDescription</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="<?php echo escape($result['OrganiserDescription']); ?>" name="OrganiserDescription" id="OrganiserDescription">
                    </div>
                </div>
            </div>
            
            <div class="alert alert-success" role="alert" style="margin-top:10px" id="feedback">
              <div class="text-center">
                <?php
                if (isset($_POST['submit']) && $statement) {
                    echo '<h4 class="alert-heading">Well done!</h4>'."{$NewEvent['Name']} Event is Updated.";
                }
                ?>
            </div>
        </div>
        </div>
</form>
        
        <div class="col-md-5" style='height: 100%;'>
            <table class="table table-striped table-hover table-bordered" >
                	<thead>
					<tr>
						<th>Select     </th>
						<th>Name   </th>
						<th>Location</th>
					</tr>
				</thead>
				<tbody>
				 <?php
				    if ($result && $statement->rowCount() > 0) {
				    foreach ($result as $row) {
				?>
				    <tr>
				    <td><a href="Update.php?Id=<?php echo escape($row['Id']);?>">-></a></td>
				    <td><?php echo escape($row['Name']);?></td>
				    <td><?php echo escape($row['Location']);?></td>
				    </tr>        
				<?php
				    }
				}
				?>
				</tbody>
            </table>
        </div>
    </div>
</div>


<?php
     include('../template/footer.php');
?>