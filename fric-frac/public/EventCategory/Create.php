<!-- Create EventCategory  -->



<?php
    include('../template/header.php');

    if (isset($_POST['submit'])) {
       include('../../config.php');
        include('../../common.php');
        $newCategory = array(
            'Name' => escape($_POST['Name'])
        );
        $statement = false;
        
        try {
            // $sql = 'INSERT INTO EventCategory (Name) VALUES (:Name)';
            $sql = sprintf("INSERT INTO %s (%s) VALUES (:%s)", 'EventCategory',
                implode(', ', array_keys($newCategory)),
                implode(', :', array_keys($newCategory)));
         
            $connection = new \PDO($host, $username, $password, $options);
            $statement = $connection->prepare($sql);
            $statement->execute($newCategory);
         
          
        } catch (\PDOException $exception) {
            echo $sql . '<br/>' . $exception->getMessage();
        }
        
    }
?>



<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">

    <div class="person container-fluid ">
        <div class="row">

            <div class="col-md-7">
                <div class="person-row row border shadow-none mb-4 bg-light rounded" style="padding:0">
                    <div class="col-md-8 ">
                        <span class="badge badge-default" style="font-size:20px; margin-top:6px">EventCategory</span>
                    </div>
                    <div class="col-4 text-right" style="padding-right:0;">
                        <button type="submit" name="submit" class="btn btn-success btn-lg">Insert</button>
                        <a href="/fric-frac/public/EventCategory/Index.php" class="btn btn-danger btn-lg" role="button">Annuleren</a>
                    </div>
                </div>


                <div class="myForm container border">
                    <ul class="list-unstyled">
                        <li>
                             <div class="form-group row">
                                <label for="Name" class="col-2 col-form-label">EventCategory</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="" name="Name" id="Name">
                                </div>
                              </div>
                        </li>

                    </ul>
                </div>

                <div class="alert alert-success" role="alert" style="margin-top:10px" id="feedback">
                    <div class="text-center">
                        <?php
                if (isset($_POST['submit']) && $statement) {
                    echo '<h4 class="alert-heading">Well done!</h4>'."{$newCategory['Name']}  is Toegevoegd.";
                }
                ?>
                    </div>
                </div>


            </div>

</form>

<div class="col-md-5" style='height: 100%;'>
    <table class="table table-striped table-hover table-bordered">
        <thead>

            <tr>
                <th>Select </th>
                <th>EventCategory Name </th>
            </tr>

        </thead>
        <tbody>
            <?php
				 if (isset($_POST['submit'])) {
           
            $statement = false;
            $sql = 'SELECT * FROM EventCategory';
            $connection = new \PDO($host, $username, $password, $options);
            $statement = $connection->prepare($sql);
            $statement->execute();
            $result = $statement->fetchAll();
        } else { 
            include('../../config.php');
           include('../../common.php');
            $statement = false;
            $sql = 'SELECT * FROM EventCategory';
            $connection = new \PDO($host, $username, $password, $options);
            $statement = $connection->prepare($sql);
            $statement->execute();
            $result = $statement->fetchAll();
                } 
				    if ($result && $statement->rowCount() > 0) {
				    foreach ($result as $row) {
				?>
            <tr>
                <td><a href="Update.php?Id=<?php echo escape($row['Id']);?>">-></a></td>
                <td>
                    <?php echo escape($row['Name']);?>
                </td>

            </tr>
            <?php
				    }
				}
				?>
        </tbody>
    </table>
</div>
</div>
<!-- </div>
</div> -->


<?php
     include('../template/footer.php');
?>