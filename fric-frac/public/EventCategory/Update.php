<!-- update EventCategory -->

<?php
    include('../template/header.php');
 ?>


<?php

if(isset($_GET['Id'])){
    include('../../config.php');
    include('../../common.php');
    $Id = escape($_GET['Id']);
    $statement = false;
    
    try {
        $sql = 'SELECT * FROM EventCategory WHERE Id = :Id';
        $connection = new \PDO($host, $username, $password, $options);
        $statement = $connection->prepare($sql);
        $statement->bindParam(':Id', $Id);
        $statement->execute();
        $result = $statement->fetch(\PDO::FETCH_ASSOC);

    } catch (\PDOException $exception) {
        echo $sql . '<br/>' . $exception->getMessage();
    }
    
}

if(isset($_POST['submit'])){
    include('../../config.php');
    include('../../common.php');
    $EventCategory = array(
            'Id'    => escape($_POST['Id'   ]),
            'Name'  => escape($_POST['Name' ])
    );
    
    try{
       $sql = 'UPDATE EventCategory SET Name = :Name
               WHERE Id = :Id';
               
        $connection = new \PDO($host, $username, $password, $options);
        $statement = $connection->prepare($sql);
        $statement -> bindParam(':Id',   $EventCategory['Id']);
        $statement -> bindParam(':Name', $EventCategory['Name']);
        $statement -> execute();
        
    }
    catch (\PDOException $exception){
        echo $sql .'<br/>' . $exception->getMessage();
    }
}

if(isset($_GET['action']) && $_GET['action'] == 'delete')
{
    $id = intval($_GET['Id']);
    $sql = 'DELETE FROM EventCategory 
               WHERE Id = :Id';
               
        $connection = new \PDO($host, $username, $password, $options);
        $statement = $connection->prepare($sql);
        $statement -> bindParam(':Id', $id);
        $statement -> execute();
        if($statement)
        {
            header('Location: Index.php');
        }
}

?>



<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
<input type="hidden" name="Id" id="Id" value="<?php echo escape($result['Id']); ?>">

<div class="person container-fluid ">
    <div class="row">
       
        <div class="col-md-7">
            <div class="person-row row border shadow-none mb-4 bg-light rounded" style="padding:0">
                <div class="col-6 ">
                    <span class="badge badge-default" style="font-size:20px; margin-top:6px">EventCategory</span>
                </div>
                
                <!-- Buttons Group -->
                
                <div class="col-6 text-right" style="padding-right:0;" >
				    <a href="/fric-frac/public/EventCategory/Index.php" class="btn btn-md btn-warning float-right" role="button">Cansel</a>
				    <a  class="btn btn-md btn-danger float-right" role="button" href="Update.php?Id=<?php echo $_GET['Id'];?>&action=delete">Delete</a>
                    <a href="/fric-frac/public/EventCategory/Create.php" class="btn btn-md btn-primary float-right" role="button">Inserting</a>
                    <input type="submit" name="submit" value="Update" class="btn btn-md btn-success float-right" role="button"/>

                 </div>
                 
                 <!-- End Buttons Group -->
            </div>
            
            <!-- Form Group -->
            
            <div class="myForm container border">
                
                <div class="form-group row">
                    <label for="Name" class="col-2 col-form-label">EventCategory Name</label>
                    <div class="col-10">
                        <input class="form-control" type="text" name="Name" id="Name" value="<?php echo escape($result['Name']); ?>">
                    </div>
                </div>
        
            </div>
            
            <!-- End Form Group -->
            
            
            <!-- Feed Back  -->

           <div class="alert alert-success" role="alert" style="margin-top:10px" id="feedback">
              <div class="text-center">
                <?php
                if (isset($_POST['submit']) && $statement) {
                    echo '<h4 class="alert-heading">Well done!</h4>'."{$EventCategory['Name']} is Updated.";
                }
                ?>
             </div>
           </div>
           
           <!-- End Feed Back  -->
            
        </div>
</form>
        
        <div class="col-md-5" style='height: 100%;'>
            <table class="table table-striped table-hover table-bordered" >
                	<thead>
					<tr>
						<th>Select       </th>
						<th>Country Name </th>
					</tr>
				</thead>
				<tbody>
				 <?php
				 $statement = false;
                try {
                    $sql = 'SELECT * FROM EventCategory';
                    $connection = new \PDO($host, $username, $password, $options);
                    $statement = $connection->prepare($sql);
                    $statement->execute();
                    $result = $statement->fetchAll();
                } catch (\PDOException $exception) {
                    echo $sql . '<br/>' . $exception->getMessage();
                }
				    if ($result && $statement->rowCount() > 0) {
				    foreach ($result as $row) {
				?>
				    <tr>
				    <td><a href="Update.php?Id=<?php echo escape($row['Id']);?>">-></a></td>
				    <td><?php echo escape($row['Name']);?></td>
				    </tr>        
				<?php
				    }
				}
				?>
				</tbody>
            </table>
        </div>
    </div>
</div>


<?php
     include('../template/footer.php');
?>