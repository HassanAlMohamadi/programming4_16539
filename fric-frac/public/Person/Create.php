
<?php
    include('../template/header.php');
 ?>


<?php

    if (isset($_POST['submit'])) {
        include('../../config.php');
        include('../../common.php');
        $NewPerson = array(
            'FirstName' => escape($_POST['FirstName' ]),
            'LastName'  => escape($_POST['LastName'  ]),
            'Email'     => escape($_POST['Email'     ]),
            'Address1'  => escape($_POST['Address1'  ]),
            'Address2'  => escape($_POST['Address2'  ]),
            'PostalCode'=> escape($_POST['PostalCode']),
            'City'      => escape($_POST['City'      ]),
            'CountryId' => intval($_POST['CountryId' ]),
            'Phone1'    => escape($_POST['Phone1'    ]),
            'Birthday'  => escape($_POST['Birthday'  ])
            /*'Rating'    => escape($_POST['Rating'    ])*/

        );
        $statement = false;
        try {
            $sql = sprintf("INSERT INTO %s (%s) VALUES (:%s)", 'Person',
                implode(', ', array_keys($NewPerson)),
                implode(', :', array_keys($NewPerson)));

            
            
            $connection = new \PDO($host, $username, $password, $options);
            $statement = $connection->prepare($sql);
             $statement->execute($NewPerson);
         
          
        } catch (\PDOException $exception) {
            echo $sql . '<br/>' . $exception->getMessage();
        }
    }
?>



<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">

<div class="person container-fluid ">
    <div class="row">
        <div class="col-md-7">
            <div class="person-row row border shadow-none mb-4 bg-light rounded" style="padding:0">
                <div class="col-md-8 ">
                    <span class="badge badge-default" style="font-size:20px; margin-top:6px">Person</span>
                </div>
                  <div class="col-4 text-right" style="padding-right:0;">
                    <button type="submit" name="submit"  class="btn btn-success btn-lg">Insert</button>
                    <a href="/fric-frac/public/Person/Index.php" class="btn btn-danger btn-lg" role="button">Annuleren</a>
                 </div>
            </div>
            
            
            <div class="myForm container border">
            <ul class="list-unstyled">
                <li>
                    <div class="form-group row">
                        <label for="FirstName" class="col-2 col-form-label">Voornaam</label>
                        <div class="col-10">
                            <input class="form-control" type="text" value="" name="FirstName" id="FirstName">
                        </div>
                      </div>
                </li>
                <li>
                <div class="form-group row">
                    <label for="LastName" class="col-2 col-form-label">Familienaam</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="" name="LastName" id="LastName">
                    </div>
                </div>
                </li>
                <li>
                <div class="form-group row">
                    <label for="Email" class="col-2 col-form-label">Email</label>
                    <div class="col-10">
                        <input class="form-control" type="email" value="" name="Email" id="Email">
                    </div>
                </div>
                </li>
                <li>
                <div class="form-group row">
                    <label for="Address1" class="col-2 col-form-label">Adres 1</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="" name="Address1" id="Address1">
                    </div>
                </div>
                </li>
                <li>
                <div class="form-group row">
                    <label for="Address2" class="col-2 col-form-label">Adres 2</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="" name="Address2" id="Address2">
                    </div>
                </div>
                </li>
                <li>
                <div class="form-group row">
                    <label for="PostalCode" class="col-2 col-form-label">PostCode</label>
                    <div class="col-10">
                        <input class="form-control" type="number" value="" name="PostalCode" id="PostalCode">
                    </div>
                </div>
                </li>
                <li>
                <div class="form-group row">
                    <label for="City" class="col-2 col-form-label">Stad</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="" name="City"  id="City">
                    </div>
                </div>
                </li>
               <div class="form-group row">
                    <label for="CountryId" class="col-2 col-form-label">Land</label>
                    <div class="col-10">
                        <select class="form-control" name="countryCode" name="CountryId" id="CountryId">
                            <option value="1">België</option>
                            <option value="2">Groot-Brittanië</option>
                            <option value="3">Nederland</option>
                            <option value="4">Duitsland</option>
                            <option value="5">Frankrijk</option>
                        </select>
                    </div> 
                </div>
                <li>
                <div class="form-group row">
                    <label for="Phone1" class="col-2 col-form-label">GSM</label>
                    <div class="col-10">
                        <input class="form-control" type="tel" value="" name="Phone1" id="Phone1">
                    </div>
                </div>
                </li>
               <div class="form-group row">
                    <label for="Birthday" class="col-2 col-form-label">Date</label>
                    <div class="col-10">
                        <input class="form-control" type="date" value="" name="Birthday" id="Birthday">
                    </div>
                </div>
            </div>
          </ul>         
            <div class="alert alert-success" role="alert" style="margin-top:10px" id="feedback">
              <div class="text-center">
                <?php
                if (isset($_POST['submit']) && $statement) {
                    echo '<h4 class="alert-heading">Well done!</h4>'."{$NewPerson['FirstName']} {$NewPerson['LastName']} is toegevoegd.";
                }
                ?>
             </div>
           </div>
   </div>
</form>
        
        <div class="col-md-5" style='height: 100%;'>
            <table class="table table-striped table-hover table-bordered" >
                	<thead>
					<tr>
						<th>Select     </th>
						<th>Voornaam   </th>
						<th>Familienaam</th>
					</tr>
				</thead>
				<tbody>
				 <?php
				 if (isset($_POST['submit'])) {
           
            $statement = false;
            $sql = 'SELECT * FROM Person';
            $connection = new \PDO($host, $username, $password, $options);
            $statement = $connection->prepare($sql);
            $statement->execute();
            $result = $statement->fetchAll();
        } else { 
            include('../../config.php');
           include('../../common.php');
            $statement = false;
            $sql = 'SELECT * FROM Person';
            $connection = new \PDO($host, $username, $password, $options);
            $statement = $connection->prepare($sql);
            $statement->execute();
            $result = $statement->fetchAll();
                } 
				    if ($result && $statement->rowCount() > 0) {
				    foreach ($result as $row) {
				?>
				    <tr>
				    <td><a href="Update.php?Id=<?php echo escape($row['Id']);?>">-></a></td>
				    <td><?php echo escape($row['FirstName']);?></td>
				    <td><?php echo escape($row['LastName']);?></td>
				    </tr>        
				<?php
				    }
				}
				?>
				</tbody>
            </table>
        </div>
    </div>
</div>


<?php
     include('../template/footer.php');
?>