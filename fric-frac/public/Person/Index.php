<!-- index person -->
<?php
    include('../template/header.php');
 ?>
 
<?php
        include('../../config.php');
        include('../../common.php');
        $statement = false;
        try {
            $sql = 'SELECT * FROM Person';
            $connection = new \PDO($host, $username, $password, $options);
            $statement = $connection->prepare($sql);
            $statement->execute();
            $result = $statement->fetchAll();
        } catch (\PDOException $exception) {
            echo $sql . '<br/>' . $exception->getMessage();
        }
?>


<div class="person container-fluid">
	<div class="row">
		<div class="col-md-7">
			<div class="person-row row border shadow-none mb-4 bg-light rounded" style="padding:0">
				<div class="col-md-10">
					<span class="badge badge-default" style="font-size:20px; margin-top:6px">Person</span>
				</div>
				<div class="col-md-2" style="padding-right: 0">
				    <a href="/fric-frac/public/Person/Create.php" class="btn btn-lg btn-primary float-right" role="button">Inserting</a>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<table class="table table-striped table-hover table-bordered">
				<thead>
					<tr>
						<th>Select     </th>
						<th>Voornaam   </th>
						<th>Familienaam</th>
					</tr>
				</thead>
				<tbody>
				 <?php
				    if ($result && $statement->rowCount() > 0) {
				    foreach ($result as $row) {
				?>
				    <tr>
				    <td><a href="Update.php?Id=<?php echo escape($row['Id']);?>">-></a></td>
				    <td><?php echo escape($row['FirstName']);?></td>
				    <td><?php echo escape($row['LastName']);?></td>
				    </tr>        
				<?php
				    }
				}
				?>
				</tbody>
			</table>
		</div>
	</div>
</div>


<?php
     include('../template/footer.php');
?>