<!-- update person page -->

<?php
    include('../template/header.php');
 ?>


<?php

if(isset($_GET['Id'])){
    include('../../config.php');
    include('../../common.php');
    $Id = escape($_GET['Id']);
    $statement = false;
    
    try {
        $sql = 'SELECT * FROM Person WHERE Id = :Id';
        $connection = new \PDO($host, $username, $password, $options);
        $statement = $connection->prepare($sql);
        $statement->bindParam(':Id', $Id);
        $statement->execute();
        $result = $statement->fetch(\PDO::FETCH_ASSOC);

    } catch (\PDOException $exception) {
        echo $sql . '<br/>' . $exception->getMessage();
    }
    
}

if(isset($_POST['submit'])){
    include('../../config.php');
    include('../../common.php');
    $person = array(
            'Id'        => escape($_POST['Id'        ]),
            'FirstName' => escape($_POST['FirstName' ]),
            'LastName'  => escape($_POST['LastName'  ]),
            'Email'     => escape($_POST['Email'     ]),
            'Address1'  => escape($_POST['Address1'  ]),
            'Address2'  => escape($_POST['Address2'  ]),
            'PostalCode'=> escape($_POST['PostalCode']),
            'City'      => escape($_POST['City'      ]),
           /* 'CountryId' => escape($_POST['CountryId' ]),*/
            'Phone1'    => escape($_POST['Phone1'    ]),
            'Birthday'  => escape($_POST['Birthday'  ])
            /*'Rating'    => escape($_POST['Rating'    ])*/
    );
    
    try{
       $sql = 'UPDATE Person SET FirstName = :FirstName, LastName = :LastName, Email = :Email, Address1 = :Address1, Address2 = :Address2,
       PostalCode = :PostalCode, City = :City, Phone1 = :Phone1, Birthday = :Birthday
               WHERE Id = :Id';
               
        $connection = new \PDO($host, $username, $password, $options);
        $statement = $connection->prepare($sql);
        $statement -> bindParam(':Id',        $person['Id']);
        $statement -> bindParam(':FirstName', $person['FirstName']);
        $statement -> bindParam(':LastName',  $person['LastName']);
        $statement -> bindParam(':Email',     $person['Email']);
        $statement -> bindParam(':Address1',  $person['Address1']);
        $statement -> bindParam(':Address2',  $person['Address2']);
        $statement -> bindParam(':PostalCode',$person['PostalCode']);
        $statement -> bindParam(':City',      $person['City']);
       /* $statement -> bindParam(':CountryId', $person['CountryId']);*/
        $statement -> bindParam(':Phone1',    $person['Phone1']);
        $statement -> bindParam(':Birthday',  $person['Birthday']);
        /*$statement -> bindParam(':Rating',    $person['Rating']);*/
        $statement -> execute();
        
    }
    catch (\PDOException $exception){
        echo $sql .'<br/>' . $exception->getMessage();
    }
}

if(isset($_GET['action']) && $_GET['action'] == 'delete')
{
    $id = intval($_GET['Id']);
    $sql = 'DELETE FROM Person 
               WHERE Id = :Id';
               
        $connection = new \PDO($host, $username, $password, $options);
        $statement = $connection->prepare($sql);
        $statement -> bindParam(':Id',        $id);
        $statement -> execute();
        if($statement)
        {
            header('Location: Index.php');
        }
}

?>



<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
<input type="hidden" name="Id" id="Id" value="<?php echo escape($result['Id']); ?>">
<div class="person container-fluid ">
    <div class="row">
        <div class="col-md-7">
            <div class="person-row row border shadow-none mb-4 bg-light rounded" style="padding:0">
                <div class="col-6 ">
                    <span class="badge badge-default" style="font-size:20px; margin-top:6px">Person</span>
                </div>
                <div class="col-6 text-right" style="padding-right:0;" >
				    <a href="/fric-frac/public/Person/Index.php" class="btn btn-md btn-warning float-right" role="button">Cansel</a>
				    <a  class="btn btn-md btn-danger float-right" role="button" href="Update.php?Id=<?php echo $_GET['Id'];?>&action=delete">Delete</a>
                    <a href="/fric-frac/public/Person/Create.php" class="btn btn-md btn-primary float-right" role="button">Inserting</a>
                    <input type="submit" name="submit" value="Update" class="btn btn-md btn-success float-right" role="button"/>

                 </div>
            </div>
            
            <div class="myForm container border">
                <div class="form-group row">
                    <label for="FirstName" class="col-2 col-form-label">Voornaam</label>
                    <div class="col-10">
                        <input class="form-control" type="text" name="FirstName" id="FirstName" value="<?php echo escape($result['FirstName']); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="LastName" class="col-2 col-form-label">Familienaam</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="<?php echo escape($result['LastName']); ?>" name="LastName" id="LastName">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Email" class="col-2 col-form-label">Email</label>
                    <div class="col-10">
                        <input class="form-control" type="email" value="<?php echo escape($result['Email']); ?>" name="Email" id="Email">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Address1" class="col-2 col-form-label">Adres 1</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="<?php echo escape($result['Address1']); ?>" name="Address1" id="Address1">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Address2" class="col-2 col-form-label">Adres 2</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="<?php echo escape($result['Address2']); ?>" name="Address2" id="Address2">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="PostalCode" class="col-2 col-form-label">PostCode</label>
                    <div class="col-10">
                        <input class="form-control" type="number" value="<?php echo escape($result['PostalCode']); ?>" name="PostalCode" id="PostalCode">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="City" class="col-2 col-form-label">Stad</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="<?php echo escape($result['City']); ?>" name="City"  id="City">
                    </div>
                </div>
               <!-- <div class="form-group row">
                    <label for="CountryId" class="col-2 col-form-label">Land</label>
                    <div class="col-10">
                        <select class="form-control" name="countryCode" name="CountryId" id="CountryId"  value="">
                            <option value="1">België</option>
                            <option value="2">Groot-Brittanië</option>
                            <option value="3">Nederland</option>
                            <option value="4">Duitsland</option>
                            <option value="5">Frankrijk</option>
                        </select>
                    </div>
                </div> -->
                 <!-- <div class="form-group row">
                    <label for="CountryId" class="col-2 col-form-label">Land</label>
                    <div class="col-10">
                           <?php
                        $selection=array('België','Groot-Brittanië','Nederland');
                        echo '<select class="form-control" name="countryCode" name="CountryId" id="CountryId">
                              <option value="0">Please Select Option</option>';
                        
                        foreach($selection as $selection){
                            $selected=($options == $selection)? "selected" : "";
                        echo '<option '.$selected.' value="'.$selection.'">'.$selection.'</option>';
                            }
                        
                        echo '</select>';
                       ?>
                    </div> 
                </div>-->
                <div class="form-group row">
                    <label for="Phone1" class="col-2 col-form-label">GSM</label>
                    <div class="col-10">
                        <input class="form-control" type="tel" value="<?php echo escape($result['Phone1']); ?>" name="Phone1" id="Phone1">
                    </div>
                </div>
               <div class="form-group row">
                    <label for="Birthday" class="col-2 col-form-label">Gebortdatum</label>
                    <div class="col-10">
                        <input class="form-control" type="date" value="<?php echo escape($result['Birthday']); ?>" name="Birthday" id="Birthday">
                    </div>
                </div> 
                
            </div>
            
           <div class="alert alert-success" role="alert" style="margin-top:10px" id="feedback">
              <div class="text-center">
                <?php
                if (isset($_POST['submit']) && $statement) {
                    echo '<h4 class="alert-heading">Well done!</h4>'."{$person['FirstName']} {$person['LastName']} is Updated.";
                }
                ?>
             </div>
           </div>
            
        </div>
</form>
        
        <div class="col-md-5" style='height: 100%;'>
            <table class="table table-striped table-hover table-bordered" >
                	<thead>
					<tr>
						<th>Select     </th>
						<th>Voornaam   </th>
						<th>Familienaam</th>
					</tr>
				</thead>
				<tbody>
				 <?php
				 $statement = false;
                try {
                    $sql = 'SELECT * FROM Person';
                    $connection = new \PDO($host, $username, $password, $options);
                    $statement = $connection->prepare($sql);
                    $statement->execute();
                    $result = $statement->fetchAll();
                } catch (\PDOException $exception) {
                    echo $sql . '<br/>' . $exception->getMessage();
                }
				    if ($result && $statement->rowCount() > 0) {
				    foreach ($result as $row) {
				?>
				    <tr>
				    <td><a href="Update.php?Id=<?php echo escape($row['Id']);?>">-></a></td>
				    <td><?php echo escape($row['FirstName']);?></td>
				    <td><?php echo escape($row['LastName']);?></td>
				    </tr>        
				<?php
				    }
				}
				?>
				</tbody>
            </table>
        </div>
    </div>
</div>


<?php
     include('../template/footer.php');
?>