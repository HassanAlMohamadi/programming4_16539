if (empty($_FILES['Image'])) {
        throw new Exception('Image file is missing');
    }
    $Image = $_FILES['Image'];
    // check INI error
    if ($Image['error'] !== 0) {
        if ($Image['error'] === 1) 
            throw new Exception('Max upload size exceeded');
            
        throw new Exception('Image uploading error: INI Error');
    }
    // check if the file exists
    if (!file_exists($Image['tmp_name']))
        throw new Exception('Image file is missing in the server');
    $maxFileSize = 2 * 10e6; // in bytes
    if ($Image['size'] > $maxFileSize)
        throw new Exception('Max size limit exceeded'); 
    // check if uploaded file is an Image
    $ImageData = getImagesize($Image['tmp_name']);
    if (!$ImageData) 
        throw new Exception('Invalid Image');
    $mimeType = $ImageData['mime'];
    // validate mime type
    $allowedMimeTypes = ['Image/jpeg', 'Image/png', 'Image/gif'];
    if (!in_array($mimeType, $allowedMimeTypes)) 
        throw new Exception('Only JPEG, PNG and GIFs are allowed');
    