<!DOCTYPE HTML>  
<html>
<head>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>  

<?php

$firstname = $lastname = $address1 = $address2 = $postcode = $city =  $email = $birthday =  $satisfied = $sex = $faculty = "";    
$firstnameErr = $lastnameErr = $address1Err = $address2Err = $postcodeErr = $cityErr =  $emailErr = "";   

if ($_SERVER["REQUEST_METHOD"] == "POST"){
  if (empty($_POST["firstname"])) {
    $firstnameErr = "Name is required";
  } else {
    $firstname = test_input($_POST["firstname"]);
     if (!preg_match("/^[a-zA-Z ]*$/",$firstname)) {
      $firstnameErr = "Only letters Allowed"; 
    }
  }
  
  if (empty($_POST["lastname"])) {
    $lastnameErr = "LastName is required";
  } else {
    $lastname = test_input($_POST["lastname"]);
     if (!preg_match("/^[a-zA-Z ]*$/",$lastname)) {
      $lastnameErr = "Only letters Allowed"; 
    }
  }
  
  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format"; 
    }
  }
  
    if (empty($_POST["address1"])) {
        $address1Err = "Address is required";
      } else {
        $address1 = test_input($_POST["address1"]);
         if (!preg_match("/^[a-zA-Z ]*$/",$address1)) {
          $address1Err = "Only letters Allowed"; 
        }
     }
  
       if (empty($_POST["city"])) {
        $cityErr = "City is required";
      } else {
        $city = test_input($_POST["city"]);
         if (!preg_match("/^[a-zA-Z ]*$/",$city)) {
          $cityErr = "Only letters Allowed"; 
        }
     }
  
     if (empty($_POST["postcode"])) {
        $postcodeErr = "Postcode is required";
      } else {
        $postcode = test_input($_POST["postcode"]);
         if (preg_match("1234567890",$postcode)) {
          $postcodeErr = "Only Nummers Allowed"; 
        }
      }
  
  $address2    = test_input($_POST["address2"]);
  $birthday    = test_input($_POST["birthday"]);
  $satisfied   = test_input($_POST["satisfied"]);
  $sex         = test_input($_POST["sex"]);
  $faculty     = test_input($_POST["faculty"]);
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

<h2 >Vul Het Registeratierformulier in. Veelden Met ee *zijn Verplicht</h2>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
        <fieldset>
            <legend>Accountgegevens</legend>
            
            <div>
                <label for="firstname">VoorNaam<span>*</span></label>
                <input type="text" name="firstname" id="firstname" required="required"/>
                <span class="warring"><?php echo $firstnameErr;?></span>
            </div>     
            
            <div>
                 <label for="lastname">Naam<span>*</span></label>
                <input type="text" name="lastname" id="lastname" required="required"/>
                <span class="warring"><?php echo $lastnameErr;?></span>
            </div>
            
            <div>
                <label for="email">E-mail adres<span>*</span></label>
                <input type="email" name="email" id="email" placeholder="Name@Example.com" required="required"/>
            </div>
            
            <div>
                <label for="password1">Wachtwoord<span>*</span></label>
                <input type="password" name="password1" id="password1" required="required"/>
            </div>
            
            <div>
                <label for="password2">Wachtwoord bevestigen<span>*</span></label>
                <input type="password" name="password2" id="password2" required="required"/>
            </div>
        </fieldset>
        
        <fieldset>
            <legend>Adresgegevens</legend>
            <div>
                <label for="address1">Adres 1<span>*</span></label>
                <input type="text" name="address1" id="address1" required="required" />
                <span class="warring"><?php echo $address1Err;?></span>
            </div>
            <div>
                <label for="address2">Adres 2</label>
                <input type="text" name="address2" id="address2" />
            </div>
            <div>
                <label for="city">Stad<span>*</span></label>
                <input type="text" name="city" id="city" required="required" />
                <span class="warring"><?php echo $cityErr;?></span>
            </div>
            <div>
                <label for="postcode">Postcode<span>*</span></label>
                <input type="text" name="postcode" id="postcode" required="required" />
                <span class="warring"><?php echo $postcodeErr;?></span>
            </div>
       
        </fieldset>
        
        <fieldset>
            <legend>Persoonlijke gegevens</legend>
            <div>
                <label for="birthday">Geboortedatum</label>
                <input type="date" name="birthday" id="birthday"  />
            </div>
            
            <div>
                <label for="satisfied">Hoe tevreden ben je?</label>
                <input type="range" name="satisfied" id="satisfied" min="-5" max="5" step="1" value="0" />
            </div>
            
            <div >
                <label for="male">Man</label>
                <input type="radio" name="sex" id="male" value="man" /> <br>
                <label for="female">Vrouw</label>
                <input type="radio" name="sex" id="female" value="vrouw" />
            </div>
            
            <div>
                <label for="faculty" style="padding-top:50px">Kies een afdeling</label>
                <select name="faculty" id="faculty" size="4">
                    <option value="Multimedia">Multimedia</option>
                    <option value="Databanken">Databanken</option>
                    <option value="Programmeren4">Programmeren4</option>
                    <option value="Programmeren5">Programmeren5</option>
                </select>
            </div>
        
        </fieldset>
        
        <fieldset class="Button">
        <button type="submit" name="action" id="submit" value="send">Verzenden</button>
        </fieldset>
    </form>
<<<<<<< HEAD

=======
<?php echo" Welcome  {$_POST['firstname']} {$_POST['lastname']}" ?><br>
Your Email is: <?php echo $_POST["email"]; ?><br>
Your Gebortdatum is: <?php echo $_POST["birthday"]; ?><br>
Your Sex is: <?php echo $_POST["sex"]; ?><br>
Your Address is: <?php echo $_POST["address1"]; ?><br>
Your Address 2 is: <?php echo $_POST["address2"]; ?><br>
<?php echo" Your City En PostCode is: {$_POST['city']} - {$_POST['postcode']}" ?><br>
Your Afdeling: <?php echo $_POST["faculty"]; ?><br>
>>>>>>> 6b41d5168cc5a715bead21b4175f47e7d7304632
</body>
</html>