<?php

// declaratie van de variabelen om geen fout te krijgen
// in de echo op het formulier
// $voornaam = $familienaam = null;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $voornaam = $_POST["voornaam"];
  $familienaam = $_POST["familienaam"];
  setcookie("voornaam", $voornaam, time() + 60);
  setcookie("familienaam", $familienaam, time() + 60);
}
else{
    if (isset($_COOKIE["voornaam"])) {
        $voornaam = $_COOKIE["voornaam"];
    }
    if (isset($_COOKIE["familienaam"])) {
        $familienaam = $_COOKIE["familienaam"];
    }
}

// setcookie('voornaam', 'Mo', time() + 86400 * 2);
// setcookie('familienaam', 'Mo', time() + 86400 * 2);
// set the expiration date to one hour ago


?>

<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Leren werken met cookies</title>
        <!Js COOKIES>
       <script>
        function setCookie(elem, expiryTime) {
            // document.cookie
            if (!String.isNullOrEmpty(elem.value)) {
                var d = new Date();
                d.setTime(d.getTime() + (expiryTime * 1000));
                var expires = "expires=" + d.toUTCString();
                document.cookie = `${elem.name}=${elem.value};${expires};path=/`;
            }
            else {
                document.cookie = `${elem.name}='';${expires};path=/`;
            }
        }


        // See if there is something in the string
        String.isNullOrEmpty = function (value) {
            return (!value || value == undefined || value == "" || value.length == 0);
        }
        
        window.onbeforeunload = function() {
            setCookie(document.querySelector('#voornaam'), 60);
            setCookie(document.querySelector('#familienaam'), 60);
        }
    </script>
</head>
<body>
<pre>
    <?php
        var_dump($_COOKIE);
    ?>
</pre>

<form action="" method="post">
    <!-- ternary operator in PHP -->
    <div><label for="voornaam">Voornaam: </label><input type="text" name="voornaam" id="voornaam" 
    value="<?php echo isset($voornaam) ? $voornaam : '';?>"></div>
    <div><label for="familienaam">Familienaam: </label><input type="text" name="familienaam" id="familienaam" 
    value="<?php echo isset($familienaam) ? $familienaam : '';?>"></div>
    <input type="submit" value="ingeven">
</form>
</body>
</html>