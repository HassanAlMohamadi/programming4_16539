<?php
namespace ModernWays\MoeMaarTevreden;

class Setup {
    /**
     * initialize tables
     */
     public static function fillCuriosityFromJSON() {
        $list = json_decode(file_get_contents(__DIR__ . '/../../../../data/neolithicum.json'), true);
        $curiosityList = $list['Curiosity'];

        foreach ($curiosityList as $item) {
            //Remove the "Id" element, which has the index "Id".
            // Om te inserten hebben we geen Id nodig
            unset($item['Id']);    
            // print_r($item);
            \ModernWays\Dal::create('Curiosity', $item);
            echo \ModernWays\Dal::getMessage() . '<br />';
        }
     }
}
